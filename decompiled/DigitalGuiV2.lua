DigitalGui._timers = {}
function DigitalGui.init(_ARG_0_, ...)
	_ARG_0_._in_tab_screen = tweak_data.hoxhud.tab_screen_timers.digitalgui or managers.user:get_setting("hoxhud_tabscreen_migrate") < 500
	DigitalGui._timers[(...):key()] = _ARG_0_
	return _UPVALUE0_.init(_ARG_0_, ...)
end
function DigitalGui.check_is_dupe(_ARG_0_)
	if _ARG_0_._isDupe then
		return true
	end
	for _FORV_4_, _FORV_5_ in pairs(DigitalGui._timers) do
		if _FORV_4_ == _ARG_0_._unit:key() then
			return false
		elseif Network:is_server() and _FORV_5_._timer == _ARG_0_._timer or Network:is_client() and string.format("%.1f", _FORV_5_._timer) == string.format("%.1f", _ARG_0_._timer) then
			_FORV_5_._isDupe = true
			if _FORV_5_._hud_timer then
				managers.hud:del_hud_timer(_FORV_5_._in_tab_screen, _FORV_5_._hud_timer)
				_FORV_5_._hud_timer = nil
			end
			return false
		end
	end
	return false
end
function DigitalGui.timer_start_count_down(_ARG_0_, ...)
	if not _ARG_0_:check_is_dupe() then
		_ARG_0_._hud_timer = _ARG_0_._hud_timer or managers.hud:add_hud_timer(_ARG_0_._in_tab_screen, tweak_data.hoxhud.timer_name_map.digitalgui, {tweak_name = "digitalgui"})
	end
	_UPVALUE0_.timer_start_count_down(_ARG_0_, ...)
end
function DigitalGui.timer_set(_ARG_0_, ...)
	if not _ARG_0_._total_time or (...) > _ARG_0_._total_time then
		_ARG_0_._total_time = (...)
	end
	_UPVALUE0_.timer_set(_ARG_0_, ...)
end
function DigitalGui.timer_pause(_ARG_0_, ...)
	if _ARG_0_._hud_timer then
		managers.hud:del_hud_timer(_ARG_0_._in_tab_screen, _ARG_0_._hud_timer)
		_ARG_0_._hud_timer = nil
	end
	_UPVALUE0_.timer_pause(_ARG_0_, ...)
end
function DigitalGui.timer_resume(_ARG_0_, ...)
	if not _ARG_0_:check_is_dupe() then
		_ARG_0_._hud_timer = _ARG_0_._hud_timer or managers.hud:add_hud_timer(_ARG_0_._in_tab_screen, tweak_data.hoxhud.timer_name_map.digitalgui, {tweak_name = "digitalgui"})
	end
	_UPVALUE0_.timer_resume(_ARG_0_, ...)
end
function DigitalGui._update_timer_text(_ARG_0_, ...)
	if not _ARG_0_._hud_timer or _ARG_0_:check_is_dupe() then
		return _UPVALUE0_._update_timer_text(_ARG_0_, ...)
	end
	_UPVALUE0_._update_timer_text(_ARG_0_, ...)
	if not _ARG_0_._total_time or _ARG_0_._total_time < _ARG_0_._timer then
		_ARG_0_._total_time = _ARG_0_._timer
	end
	if _ARG_0_._hud_timer then
		if _ARG_0_._timer <= 0.19 then
			managers.hud:del_hud_timer(_ARG_0_._in_tab_screen, _ARG_0_._hud_timer)
			_ARG_0_._hud_timer = nil
		else
			_ARG_0_._hud_timer:set_completion(_ARG_0_._timer, _ARG_0_._total_time)
		end
	end
end
function DigitalGui._timer_stop(_ARG_0_, ...)
	if _ARG_0_._hud_timer then
		managers.hud:del_hud_timer(_ARG_0_._in_tab_screen, _ARG_0_._hud_timer)
		_ARG_0_._hud_timer = nil
	end
	_UPVALUE0_._timer_stop(_ARG_0_, ...)
end
function DigitalGui.destroy(_ARG_0_, ...)
	DigitalGui._timers[_ARG_0_._unit:key()] = nil
	_UPVALUE0_.destroy(_ARG_0_, ...)
end
