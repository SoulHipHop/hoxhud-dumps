HUDAssaultCorner._info_boxes = {}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.pagers_info_box.order] = {
	name = "pagers",
	rect = {
		64,
		256,
		64,
		64
	},
	want = "_nr_pagers",
	tweak_data = tweak_data.hoxhud.pagers_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.bodybags_info_box.order] = {
	name = "bodybags",
	rect = {
		448,
		128,
		64,
		64
	},
	want = "_nr_bodybags",
	tweak_data = tweak_data.hoxhud.bodybags_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.dom_info_box.order] = {
	name = "dominated",
	rect = {
		128,
		512,
		64,
		64
	},
	want = "_nr_dominated",
	tweak_data = tweak_data.hoxhud.dom_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.jokers_info_box.order] = {
	name = "jokers",
	rect = {
		384,
		512,
		64,
		64
	},
	want = "_nr_jokered",
	tweak_data = tweak_data.hoxhud.jokers_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.sentry_info_box.order] = {
	name = "sentries",
	rect = {
		448,
		320,
		64,
		64
	},
	want = "_nr_sentries",
	tweak_data = tweak_data.hoxhud.sentry_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.feedback_info_box.order] = {
	name = "feedback",
	rect = {
		384,
		128,
		64,
		64
	},
	want = "_nr_feedback",
	tweak_data = tweak_data.hoxhud.feedback_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.alertedcivs_info_box.order] = {
	name = "alertedcivs",
	rect = {
		384,
		448,
		64,
		64
	},
	want = "_nr_alertedcivs",
	tweak_data = tweak_data.hoxhud.alertedcivs_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.totalenemies_info_box.order] = {
	name = "totalenemies",
	rect = {
		384,
		64,
		64,
		64
	},
	want = "_nr_enemies",
	tweak_data = tweak_data.hoxhud.totalenemies_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.lootbags_info_box.order] = {
	name = "lootbags",
	rect = {
		128,
		192,
		64,
		64
	},
	want = "_nr_lootbags",
	tweak_data = tweak_data.hoxhud.lootbags_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.gagemodpack_info_box.order] = {
	name = "gagemodpack",
	rect = {
		0,
		512,
		64,
		64
	},
	want = "_nr_gagepacks",
	tweak_data = tweak_data.hoxhud.gagemodpack_info_box
}
HUDAssaultCorner._info_box_padding = 7
HUDAssaultCorner._icon_size = 38
function HUDAssaultCorner.InjectImprovedInfoBoxes(_ARG_0_)
	_ARG_0_._hud_panel:child("hostages_panel"):set_h(_ARG_0_._hud_panel:child("hostages_panel"):h() + 290)
	for _FORV_5_, _FORV_6_ in ipairs(_ARG_0_._info_box_panels) do
		HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
			w = 38,
			h = 38,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
		}, {}):set_right(_ARG_0_._hud_panel:child("hostages_panel"):w())
		_ARG_0_._hud_panel:child("hostages_panel"):bitmap({
			name = _FORV_6_.name .. "_icon",
			texture = _FORV_6_.icon or "guis/textures/pd2/skilltree/icons_atlas",
			valign = "top",
			texture_rect = _FORV_6_.rect,
			color = _FORV_6_.color or Color.white,
			layer = 1,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding,
			h = _ARG_0_._icon_size + (_FORV_6_.extra or 0),
			w = _ARG_0_._icon_size + (_FORV_6_.extra or 0)
		}):set_right(HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
			w = 38,
			h = 38,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
		}, {}):left())
		_FORV_6_.icon, _FORV_6_.num_text, _FORV_6_.bg_box = _ARG_0_._hud_panel:child("hostages_panel"):bitmap({
			name = _FORV_6_.name .. "_icon",
			texture = _FORV_6_.icon or "guis/textures/pd2/skilltree/icons_atlas",
			valign = "top",
			texture_rect = _FORV_6_.rect,
			color = _FORV_6_.color or Color.white,
			layer = 1,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding,
			h = _ARG_0_._icon_size + (_FORV_6_.extra or 0),
			w = _ARG_0_._icon_size + (_FORV_6_.extra or 0)
		}), HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
			w = 38,
			h = 38,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
		}, {}):text({
			name = "num_" .. _FORV_6_.name,
			text = "0",
			valign = "center",
			align = "center",
			vertical = "center",
			w = HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
				w = 38,
				h = 38,
				x = 0,
				y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
			}, {}):w(),
			h = HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
				w = 38,
				h = 38,
				x = 0,
				y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
			}, {}):h(),
			layer = 1,
			x = 0,
			y = 0,
			color = Color.white,
			font = tweak_data.hud_corner.assault_font,
			font_size = tweak_data.hud_corner.numhostages_size
		}), HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
			w = 38,
			h = 38,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
		}, {})
	end
end
function HUDAssaultCorner.layout_info_boxes(_ARG_0_, _ARG_1_)
	if managers.user:get_setting("hoxhud_anticheat_only") then
		return
	end
	_ARG_0_._hud_panel:child("hostages_panel"):set_w((38 + _ARG_0_._icon_size + 2) * math.round(managers.user:get_setting("hoxhud_infoboxes_per_row") or 2) - 2)
	_ARG_0_._hud_panel:child("hostages_panel"):set_x(_ARG_0_._hud_panel:child("hostages_panel"):x() - (_ARG_0_._hud_panel:child("hostages_panel"):w() - _ARG_0_._hud_panel:child("hostages_panel"):w()))
	_ARG_0_._info_box_panels[0].icon:set_x(0)
	_ARG_0_._info_box_panels[0].bg_box:set_x(_ARG_0_._info_box_panels[0].icon:right())
	for _FORV_13_, _FORV_14_ in ipairs(_ARG_0_._info_box_panels) do
		if _FORV_14_ and not _FORV_14_.tweak_data.hide and (_ARG_1_ == "stealth" and not _FORV_14_.tweak_data.loud_only or _ARG_1_ == "loud" and not _FORV_14_.tweak_data.stealth_only) and (not _FORV_14_.tweak_data.hideAtZero or _FORV_14_.num_text:text() ~= "0") then
			_FORV_14_.icon:set_x((38 + _ARG_0_._icon_size + 2) * (math.round(managers.user:get_setting("hoxhud_infoboxes_per_row") or 2) > 1 and 1 or 0))
			_FORV_14_.icon:set_y(math.round(managers.user:get_setting("hoxhud_infoboxes_per_row") or 2) > 1 and _ARG_0_._info_box_panels[0].bg_box:y() or _ARG_0_._info_box_panels[0].bg_box:bottom() + _ARG_0_._info_box_padding)
			_FORV_14_.bg_box:set_x(_FORV_14_.icon:right())
			_FORV_14_.bg_box:set_y(math.round(managers.user:get_setting("hoxhud_infoboxes_per_row") or 2) > 1 and _ARG_0_._info_box_panels[0].bg_box:y() or _ARG_0_._info_box_panels[0].bg_box:bottom() + _ARG_0_._info_box_padding)
			_FORV_14_.icon:set_visible(true)
			_FORV_14_.bg_box:set_visible(true)
			if (math.round(managers.user:get_setting("hoxhud_infoboxes_per_row") or 2) > 1 and 1 or 0) + 1 == math.round(managers.user:get_setting("hoxhud_infoboxes_per_row") or 2) then
			end
		else
			_FORV_14_.bg_box:set_visible(false)
			_FORV_14_.icon:set_visible(false)
		end
	end
	_ARG_0_._info_box_panels[_FORV_13_].bg_box:set_right(_ARG_0_._hud_panel:child("hostages_panel"):w())
	_ARG_0_._info_box_panels[_FORV_13_].icon:set_right(_ARG_0_._info_box_panels[_FORV_13_].bg_box:left())
	for _FORV_14_ = -_FORV_13_ + 1, -(_FORV_13_ or 0) do
		_FORV_14_ = math.abs(_FORV_14_)
		if _ARG_0_._info_box_panels[_FORV_14_] and _ARG_0_._info_box_panels[_FORV_14_].icon:visible() then
			_ARG_0_._info_box_panels[_FORV_14_].bg_box:set_right(_ARG_0_._info_box_panels[_FORV_13_].icon:left() - 2)
			_ARG_0_._info_box_panels[_FORV_14_].icon:set_right(_ARG_0_._info_box_panels[_FORV_14_].bg_box:left())
		end
	end
end
function HUDAssaultCorner.init(_ARG_0_, ...)
	_UPVALUE0_.init(_ARG_0_, ...)
	_ARG_0_._info_box_panels = _ARG_0_._info_boxes
	if managers.user:get_setting("hoxhud_anticheat_only") then
		_ARG_0_._info_box_panels[0] = {
			bg_box = _ARG_0_._hostages_bg_box,
			icon = _ARG_0_._hud_panel:child("hostages_panel"):child("hostages_icon")
		}
		return
	end
	_ARG_0_._hud_panel:child("hostages_panel"):remove(_ARG_0_._hud_panel:child("hostages_panel"):child("hostages_icon"))
	_ARG_0_._info_box_panels[0] = {
		bg_box = _ARG_0_._hostages_bg_box,
		icon = _ARG_0_._hud_panel:child("hostages_panel"):bitmap({
			name = "hostages_icon",
			texture = "guis/textures/pd2/skilltree/icons_atlas",
			valign = "top",
			texture_rect = {
				256,
				448,
				64,
				64
			},
			color = Color.white,
			layer = 1,
			x = 0,
			y = 0,
			h = _ARG_0_._icon_size,
			w = _ARG_0_._icon_size
		})
	}
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._info_box_panels) do
		_FORV_7_.tweak_data.hide = not managers.user:get_setting(_FORV_7_.tweak_data.opt)
		_ARG_0_[_FORV_7_.want] = 0
	end
	_ARG_0_._nr_hostages = 0
	_ARG_0_._nr_lootbags = managers.interaction:get_current_loot_count()
	_ARG_0_._nr_bodybags = managers.player:total_body_bags()
	_ARG_0_._nr_pagers = #tweak_data.player.alarm_pager.bluff_success_chance - 1
	_ARG_0_:InjectImprovedInfoBoxes()
	_ARG_0_:layout_info_boxes(managers.groupai:state():whisper_mode() and "stealth" or "loud")
	_ARG_0_._hud_panel:child("assault_panel"):set_right(_ARG_0_._hud_panel:w() / 2 + 121)
	_ARG_0_._hud_panel:child("point_of_no_return_panel"):set_right(_ARG_0_._hud_panel:w() / 2 + 121)
	_ARG_0_._hud_panel:child("assault_panel"):child("icon_assaultbox"):set_visible(false)
	if managers.user:get_setting("hoxhud_show_heist_timer_on_assault") then
		_ARG_0_._last_timer_size = 0
		_ARG_0_._mission_timer = HUDHeistTimer:new({
			panel = _ARG_0_._bg_box:panel({
				name = "timer_panel",
				x = 4
			})
		})
		_ARG_0_._mission_timer._timer_text:set_font_size(tweak_data.hud_corner.assault_size)
		_ARG_0_._mission_timer._timer_text:set_font(Idstring(tweak_data.hud_corner.assault_font))
		_ARG_0_._mission_timer._timer_text:set_align("left")
		_ARG_0_._mission_timer._timer_text:set_vertical("center")
		_ARG_0_._mission_timer._timer_text:set_color(Color.white:with_alpha(0.9))
	end
end
function HUDAssaultCorner.get_num_jokers(_ARG_0_)
	return _ARG_0_._nr_jokered or 0
end
function HUDAssaultCorner.get_num_dominated(_ARG_0_)
	return _ARG_0_._nr_dominated or 0
end
function HUDAssaultCorner.get_num_feedback(_ARG_0_)
	return _ARG_0_._nr_feedback or 0
end
function HUDAssaultCorner._compare_and_update_infobox(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	if _ARG_1_:text() == _ARG_3_ then
		return
	end
	_ARG_1_:set_text(_ARG_3_)
	_ARG_2_:stop()
	_ARG_2_:animate(callback(nil, _G, "HUDBGBox_animate_bg_attention"), {})
end
function HUDAssaultCorner.feed_heist_time(_ARG_0_, _ARG_1_)
	if not managers.user:get_setting("hoxhud_show_heist_timer_on_assault") or not _ARG_0_._mission_timer then
		return
	end
	_ARG_0_._mission_timer:set_time(_ARG_1_)
	if _ARG_0_._bg_box:child("text_panel") and _ARG_0_._bg_box:w() >= 242 and _ARG_0_._mission_timer._timer_text:text_rect() > _ARG_0_._last_timer_size then
		_ARG_0_._last_timer_size = _ARG_0_._mission_timer._timer_text:text_rect()
		_ARG_0_._bg_box:child("text_panel"):set_w(_ARG_0_._bg_box:w() - _ARG_0_._mission_timer._timer_text:text_rect() - 8)
		_ARG_0_._bg_box:child("text_panel"):set_x(_ARG_0_._mission_timer._timer_text:text_rect() + 7)
	end
end
function HUDAssaultCorner.set_control_info(_ARG_0_, _ARG_1_)
	if managers.user:get_setting("hoxhud_anticheat_only") or not alive(_ARG_0_._hostages_bg_box) then
		return _ARG_1_.nr_hostages and _UPVALUE0_.set_control_info(_ARG_0_, _ARG_1_)
	end
	for _FORV_5_, _FORV_6_ in pairs(_ARG_1_) do
		_ARG_0_["_" .. _FORV_5_] = _ARG_0_["_" .. _FORV_5_] and _FORV_6_ or nil
	end
	_ARG_0_._nr_pagers = _ARG_1_.pager_answered and (_ARG_0_._nr_pagers - 1 >= 0 and _ARG_0_._nr_pagers - 1 or 0) or _ARG_0_._nr_pagers
	if not managers.user:get_setting("hoxhud_anticheat_only") then
		for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._info_box_panels) do
			if alive(_FORV_7_.bg_box) then
				_ARG_0_:_compare_and_update_infobox(_FORV_7_.num_text, _FORV_7_.bg_box:child("bg"), (tostring(_ARG_0_[_FORV_7_.want])))
			end
		end
	end
	_ARG_0_:_compare_and_update_infobox(_ARG_0_._hostages_bg_box:child("num_hostages"), _ARG_0_._hostages_bg_box:child("bg"), tostring(_ARG_0_._nr_hostages - _ARG_0_._nr_dominated))
	if false or _FORV_7_.tweak_data.hideAtZero and tostring(_ARG_0_[_FORV_7_.want]) ~= _FORV_7_.num_text:text() then
		_ARG_0_:layout_info_boxes(managers.groupai:state():whisper_mode() and "stealth" or "loud")
	end
end
function HUDAssaultCorner.sync_start_assault(_ARG_0_, ...)
	_UPVALUE0_.sync_start_assault(_ARG_0_, ...)
	_ARG_0_._hud_panel:child("hostages_panel"):set_visible(not managers.user:get_setting("hoxhud_anticheat_only"))
end
function HUDAssaultCorner._start_assault(_ARG_0_, _ARG_1_, ...)
	if Network:is_server() and not tweak_data.hoxhud.disable_enhanced_assault_indicator then
		for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_) do
			_ARG_1_[_FORV_6_] = _FORV_7_ == "hud_assault_assault" and "hud_assault_stats" or _FORV_7_
		end
	end
	if not _ARG_0_._bg_box:child("text_panel") and managers.user:get_setting("hoxhud_show_heist_timer_on_assault") then
		_ARG_0_._bg_box:panel({
			name = "text_panel",
			w = _ARG_0_._bg_box:w() - 100,
			x = 100
		})
	end
	return _UPVALUE0_._start_assault(_ARG_0_, _ARG_1_, ...)
end
function HUDAssaultCorner.show_point_of_no_return_timer(_ARG_0_, ...)
	_UPVALUE0_.show_point_of_no_return_timer(_ARG_0_, ...)
	_ARG_0_._hud_panel:child("hostages_panel"):set_visible(not managers.user:get_setting("hoxhud_anticheat_only"))
end
function HUDAssaultCorner.show_casing(_ARG_0_, ...)
	if managers.user:get_setting("hoxhud_anticheat_only") then
		return _UPVALUE0_.show_casing(_ARG_0_, ...)
	end
end
function HUDAssaultCorner.hide_casing(_ARG_0_, ...)
	if managers.user:get_setting("hoxhud_anticheat_only") then
		return _UPVALUE0_.hide_casing(_ARG_0_, ...)
	end
end
