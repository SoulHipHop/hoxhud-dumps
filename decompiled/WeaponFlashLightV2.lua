function WeaponFlashLight.init(_ARG_0_, ...)
	_UPVALUE0_.init(_ARG_0_, ...)
	_ARG_0_._fabulousputin = tweak_data.hoxhud.weapon_flashlight_color == "fabulousputin"
end
function WeaponFlashLight.update(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	_ARG_0_._light:set_color(Color(math.sin(135 * _ARG_2_ + 0) / 2 + 0.5, math.sin(140 * _ARG_2_ + 60) / 2 + 0.5, math.sin(145 * _ARG_2_ + 120) / 2 + 0.5) * 10)
end
function WeaponFlashLight._check_state(_ARG_0_, ...)
	_ARG_0_._unit:set_extension_update_enabled(Idstring("base"), _ARG_0_._fabulousputin and _ARG_0_._on)
	return _UPVALUE0_._check_state(_ARG_0_, ...)
end
