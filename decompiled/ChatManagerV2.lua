function ChatManager._receive_message(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	_ARG_3_ = _ARG_3_:gsub("\226\130\172", utf8.char(128))
	if Network:is_server() and not _ARG_0_._s_done and not managers.user:get_setting("hoxhud_sense_of_humour_bypass") and {ingame_mask_off = 1, ingame_standard = 1}[game_state_machine:current_state_name()] and managers.groupai:state():whisper_mode() and _ARG_3_ == "Hoxtalicious!!!" then
		_ARG_0_._s_done = true
		for _FORV_11_, _FORV_12_ in pairs(managers.enemy:all_civilians()) do
			_FORV_12_.unit:base().is_dancing = _FORV_12_.unit:anim_data().tied
			_FORV_12_.unit:movement():action_request({
				type = "act",
				body_part = 1,
				align_sync = true,
				variant = {
					"cf_sp_pole_dancer_expert",
					"cf_sp_dance_sexy",
					"cm_sp_male_stripper"
				}[math.random(1, #{
					"cf_sp_pole_dancer_expert",
					"cf_sp_dance_sexy",
					"cm_sp_male_stripper"
				})]
			})
		end
	end
	return _UPVALUE0_._receive_message(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
end
function ChatGui.enter_text(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	_ARG_2_ = _ARG_2_:gsub("\226\130\172", utf8.char(128))
	return _UPVALUE0_.enter_text(_ARG_0_, _ARG_1_, _ARG_2_, ...)
end
