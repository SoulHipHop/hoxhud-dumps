function CopSound.destroy(_ARG_0_, _ARG_1_, ...)
	if _ARG_1_ and _ARG_1_:base() then
		return _UPVALUE0_.destroy(_ARG_0_, _ARG_1_, ...)
	end
end
