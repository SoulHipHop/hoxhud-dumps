function LootManager.sync_load(_ARG_0_, ...)
	_UPVALUE0_.sync_load(_ARG_0_, ...)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._global.secured) do
		_FORV_7_.multiplier = (_FORV_7_.multiplier > tweak_data.upgrades.values.player.small_loot_multiplier[#tweak_data.upgrades.values.player.small_loot_multiplier] or _FORV_7_.multiplier < 1) and 1 or _FORV_7_.multiplier
	end
end
function LootManager.would_be_bonus_bag(_ARG_0_, _ARG_1_)
	if (_ARG_0_._global.mandatory_bags.amount or 0) == 0 then
		return true
	end
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._global.secured) do
		if not tweak_data.carry.small_loot[_FORV_7_.carry_id] then
			if 0 < (_ARG_0_._global.mandatory_bags.amount or 0) and (_ARG_0_._global.mandatory_bags.carry_id == "none" or _ARG_0_._global.mandatory_bags.carry_id == _FORV_7_.carry_id) then
			elseif (_ARG_0_._global.mandatory_bags.amount or 0) - 1 == 0 then
				return true
			end
		end
	end
	return (_ARG_0_._global.mandatory_bags.amount or 0) - 1 == 0
end
function LootManager.set_loot_check_override(_ARG_0_, _ARG_1_)
	_ARG_0_._check_would_be_bonus_bag = _ARG_1_
end
function LootManager.is_bonus_bag(_ARG_0_, ...)
	return _ARG_0_._check_would_be_bonus_bag and _ARG_0_:would_be_bonus_bag(...) or _UPVALUE0_.is_bonus_bag(_ARG_0_, ...)
end
