function SentryGunDamage.die(_ARG_0_, ...)
	managers.hud:del_sentry_unit(_ARG_0_._unit)
	_UPVALUE0_.die(_ARG_0_, ...)
end
function SentryGunDamage.destroy(_ARG_0_, ...)
	managers.hud:del_sentry_unit(_ARG_0_._unit)
	_UPVALUE0_.destroy(_ARG_0_, ...)
end
