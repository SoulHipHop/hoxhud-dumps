function StatisticsManager._setup(_ARG_0_, ...)
	_UPVALUE0_._setup(_ARG_0_, ...)
	_ARG_0_._defaults.players_cumulative_dmg = {}
end
function StatisticsManager.on_enemy_damaged(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_)
	if not alive(_ARG_2_) or not managers.criminals:character_data_by_unit(_ARG_2_) or not alive(_ARG_1_) or not _ARG_1_:base() or not _ARG_1_:base()._tweak_table or _ARG_1_:base()._tweak_table:find("civilian") or not _ARG_2_:network() or not _ARG_2_:network():peer() or not _ARG_2_:network():peer():name() then
		return
	end
	if not _ARG_4_ or not _ARG_3_ then
		_ARG_3_ = _ARG_3_ * (_ARG_1_:character_damage()._HEALTH_INIT_PRECENT or 0)
	end
	_ARG_0_._global.session.players_cumulative_dmg[_ARG_2_:network():peer():name()] = (_ARG_0_._global.session.players_cumulative_dmg[_ARG_2_:network():peer():name()] or 0) + _ARG_3_
end
function StatisticsManager.clear_peer_damage_count(_ARG_0_, _ARG_1_)
	_ARG_0_._global.session.players_cumulative_dmg[_ARG_1_] = nil
end
function StatisticsManager.get_best_enemy_damage_peer(_ARG_0_)
	for _FORV_6_, _FORV_7_ in pairs(_ARG_0_._global.session.players_cumulative_dmg) do
		if _FORV_7_ > 0 then
		end
	end
	return _FORV_6_, math.round(managers.user:get_setting("hoxhud_show_enemy_health_mul") and _FORV_7_ * 10 or _FORV_7_)
end
function StatisticsManager.get_casual_enemy_damage_peer(_ARG_0_)
	for _FORV_4_, _FORV_5_ in pairs(managers.criminals:characters()) do
		if _FORV_5_.taken and alive(_FORV_5_.unit) and not _FORV_5_.data.ai and not _ARG_0_._global.session.players_cumulative_dmg[_FORV_5_.unit:network():peer():name()] then
			return _FORV_5_.unit:network():peer():name(), 0
		end
	end
	for _FORV_6_, _FORV_7_ in pairs(_ARG_0_._global.session.players_cumulative_dmg) do
		if _FORV_7_ < math.huge then
		end
	end
	return _FORV_6_, math.round(managers.user:get_setting("hoxhud_show_enemy_health_mul") and _FORV_7_ * 10 or _FORV_7_)
end
function StatisticsManager.get_personal_session_damage_total(_ARG_0_)
	return math.round(managers.user:get_setting("hoxhud_show_enemy_health_mul") and (_ARG_0_._global.session.players_cumulative_dmg[managers.network:session():local_peer():name()] or 0) * 10 or _ARG_0_._global.session.players_cumulative_dmg[managers.network:session():local_peer():name()] or 0)
end
function StatisticsManager.killed(_ARG_0_, _ARG_1_)
	if _ARG_1_.name:find("civilian") then
		_ARG_1_.head_shot = false
	end
	_UPVALUE0_.killed(_ARG_0_, _ARG_1_)
	if (_ARG_1_.weapon_unit and _ARG_1_.weapon_unit:base().get_name_id and _ARG_1_.weapon_unit:base():get_name_id() or "") == "sentry_gun" then
		_ARG_0_._sentry_headshots = (_ARG_0_._sentry_headshots or 0) + (_ARG_1_.head_shot and 1 or 0)
		_ARG_0_._sentry_kills = (_ARG_0_._sentry_kills or 0) + 1
	end
	managers.hud:update_kill_counter(HUDManager.PLAYER_PANEL, _ARG_0_:session_total_head_shots(), _ARG_0_:session_total_kills() - _ARG_0_:session_total_civilian_kills() - _ARG_0_:session_total_head_shots(), _ARG_0_._sentry_headshots, _ARG_0_._sentry_kills, deep_clone(_ARG_0_:get_session_killed()))
end
function StatisticsManager.in_custody(_ARG_0_, ...)
	managers.hud:hide_interaction_bar(false)
	managers.hud:set_enemy_health_visible(false)
	return _UPVALUE0_.in_custody(_ARG_0_, ...)
end
function StatisticsManager.get_session_killed(_ARG_0_)
	return _ARG_0_._global.session.killed
end
function StatisticsManager.get_session_melee_kills(_ARG_0_)
	return _ARG_0_._global.session.killed.total.melee
end
