function EnemyManager.on_enemy_registered(_ARG_0_, ...)
	_UPVALUE0_.on_enemy_registered(_ARG_0_, ...)
	managers.hud:set_control_info({
		nr_enemies = _ARG_0_._enemy_data.nr_units
	})
end
function EnemyManager.on_enemy_unregistered(_ARG_0_, ...)
	_UPVALUE0_.on_enemy_unregistered(_ARG_0_, ...)
	managers.hud:set_control_info({
		nr_enemies = _ARG_0_._enemy_data.nr_units
	})
end
function EnemyManager.get_number_of_enemies(_ARG_0_)
	return _ARG_0_._enemy_data.nr_units
end
function EnemyManager.get_delayed_clbk_expire_t(_ARG_0_, _ARG_1_)
	for _FORV_5_, _FORV_6_ in ipairs(_ARG_0_._delayed_clbks) do
		if _FORV_6_[1] == _ARG_1_ then
			return _FORV_6_[2]
		end
	end
end
