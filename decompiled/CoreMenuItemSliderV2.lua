core:module("CoreMenuItemSlider")
function ItemSlider.setup_gui(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	_ARG_2_.gui_slider_text:set_font_size(_G.tweak_data.menu.stats_font_size)
	return unpack({
		_UPVALUE0_.setup_gui(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	})
end
function ItemSlider.reload(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	if _ARG_0_:show_value() then
		_ARG_1_.gui_slider_text:set_text(string.format("%." .. math.abs(math.ceil(-math.log(_ARG_0_._step) / math.log(10))) .. "f", _ARG_0_:value()))
	end
	return unpack({
		_UPVALUE0_.reload(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	})
end
