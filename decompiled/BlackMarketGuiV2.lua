function BlackMarketGui.populate_characters(_ARG_0_, _ARG_1_, ...)
	_UPVALUE0_.populate_characters(_ARG_0_, _ARG_1_, ...)
	for _FORV_6_, _FORV_7_ in pairs(_ARG_1_) do
		if _FORV_7_.bitmap_texture == "guis/textures/pd2/blackmarket/icons/characters/old_hoxton" then
			_FORV_7_.bitmap_texture = "guis/textures/pd2/blackmarket/icons/masks/old_hoxton"
			break
		end
	end
end
