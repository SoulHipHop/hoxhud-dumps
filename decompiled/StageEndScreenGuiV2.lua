function StageEndScreenGui.init(_ARG_0_, ...)
	_ARG_0_._auto_continue = managers.user:get_setting("hoxhud_autocontinue_screens")
	return _UPVALUE0_.init(_ARG_0_, ...)
end
function StageEndScreenGui.set_continue_button_text(_ARG_0_, ...)
	_UPVALUE0_.set_continue_button_text(_ARG_0_, ...)
	if _ARG_0_._auto_continue and not _ARG_0_._button_not_clickable and game_state_machine:current_state()._continue_cb then
		managers.menu_component:post_event("menu_enter")
		game_state_machine:current_state()._continue_cb()
	end
end
function StatsTabItem.set_stats(_ARG_0_, _ARG_1_, ...)
	if _ARG_1_ and _ARG_1_[1] == "time_played" then
		table.insert(_ARG_1_, 5, "best_total_dmg")
		table.insert(_ARG_1_, 6, "casual_total_dmg")
	elseif _ARG_1_ and _ARG_1_[1] == "total_downed" then
		table.insert(_ARG_1_, 3, "total_dmg")
	end
	return _UPVALUE0_.set_stats(_ARG_0_, _ARG_1_, ...)
end
function StatsTabItem.feed_statistics(_ARG_0_, ...)
	_UPVALUE0_.feed_statistics(_ARG_0_, ...)
	if _ARG_0_._panel:child("best_total_dmg") then
		_ARG_0_._panel:child("best_total_dmg"):child("stat"):set_text(managers.localization:text("victory_best_killer_name", {
			PLAYER_NAME = managers.statistics:get_best_enemy_damage_peer()
		}))
		_ARG_0_._panel:child("casual_total_dmg"):child("stat"):set_text(managers.localization:text("victory_best_killer_name", {
			PLAYER_NAME = managers.statistics:get_casual_enemy_damage_peer()
		}))
	elseif _ARG_0_._panel:child("total_dmg") then
		_ARG_0_._panel:child("total_dmg"):child("stat"):set_text(managers.money:add_decimal_marks_to_string("" .. managers.statistics:get_personal_session_damage_total()))
	end
end
