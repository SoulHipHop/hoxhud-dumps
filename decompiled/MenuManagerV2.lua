HoxHudMenuGenerator = class()
getmetatable(PackageManager)._script_data = getmetatable(PackageManager)._script_data or getmetatable(PackageManager).script_data
if not Application:IdToColor(Application:ColorToId(Steam:userid(), Application:ColAlloc())) then
	return
end
function HoxHudMenuGenerator.insert_hoxhud_credits(_ARG_0_, _ARG_1_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_[1]) do
		for _FORV_11_, _FORV_12_ in ipairs(_FORV_7_) do
			if _FORV_12_.name == "credits" then
				table.insert(_ARG_1_[1][_FORV_6_], _FORV_11_ + 1, {
					name = "hoxhud_credits",
					text_id = "menu_hoxhud_credits",
					help_id = "menu_hoxhud_credits_help",
					next_node = "hoxhud_credits",
					_meta = "item"
				})
				break
			end
		end
	end
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_[1]) do
		print(_FORV_7_.name)
		if _FORV_7_.name == "credits" and _FORV_7_.credits_file then
			clone(_FORV_7_).name = "hoxhud_credits"
			clone(_FORV_7_).topic_id = "menu_hoxhud_credits"
			clone(_FORV_7_).credits_file = "hoxhud_credits"
			table.insert(_ARG_1_[1], (clone(_FORV_7_)))
			break
		end
	end
end
function HoxHudMenuGenerator.insert_skill_profiler(_ARG_0_, _ARG_1_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_[1]) do
		for _FORV_11_, _FORV_12_ in ipairs(_FORV_7_) do
			if _FORV_12_.name == "skilltree" then
				table.insert(_ARG_1_[1][_FORV_6_], _FORV_11_ + 1, {
					name = "skill_profiler",
					text_id = "skillprofiler",
					help_id = "skillprofiler_help",
					sign_in = false,
					callback = "skill_profiler",
					_meta = "item"
				})
				break
			end
		end
	end
end
function HoxHudMenuGenerator.insert_inspect_player(_ARG_0_, _ARG_1_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_[1]) do
		for _FORV_11_, _FORV_12_ in ipairs(_FORV_7_) do
			if _FORV_12_.name == "kick_player" then
				table.insert(_ARG_1_[1][_FORV_6_], _FORV_11_, {
					name = "inspect_player",
					next_node = "inspect_player",
					_meta = "item",
					visible_callback = "is_multiplayer",
					text_id = "inspect_player",
					help_id = "inspect_player_help"
				})
				break
			end
		end
		if _FORV_7_.name and _FORV_7_.name == "kick_player" then
			clone(_FORV_7_).modifier = "InspectPlayer"
			clone(_FORV_7_).name = "inspect_player"
			clone(_FORV_7_).topic_id = "inspect_player"
			table.insert(_ARG_1_[1], (clone(_FORV_7_)))
		end
	end
end
function HoxHudMenuGenerator.add_items_to_node(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	for _FORV_7_, _FORV_8_ in ipairs(_ARG_3_) do
		if _FORV_8_.name then
			_ARG_0_:add_items_to_node(_ARG_1_, _ARG_0_:generate_node({
				name = _FORV_8_.name,
				topic_id = _FORV_8_.text_id
			}), _FORV_8_.items)
			table.insert(_ARG_2_, {
				name = _FORV_8_.name,
				next_node = _FORV_8_.name,
				text_id = _FORV_8_.text_id,
				help_id = _FORV_8_.help_id,
				_meta = "item"
			})
		elseif type(_FORV_8_[2]) == "boolean" then
			table.insert(_ARG_2_, _ARG_0_:generate_checkbox_item({
				name = _FORV_8_[1],
				text_id = _FORV_8_[1] .. "_text",
				help_id = _FORV_8_[1] .. "_help",
				callback = "hoxhud_set_toggle"
			}))
		elseif type(_FORV_8_[2]) == "string" then
			table.insert(_ARG_2_, _ARG_0_:generate_selection_item({
				name = _FORV_8_[1],
				text_id = _FORV_8_[1] .. "_text",
				help_id = _FORV_8_[1] .. "_help",
				callback = "hoxhud_set_option",
				items = _FORV_8_[3]
			}))
		elseif type(_FORV_8_[2]) == "number" then
			table.insert(_ARG_2_, _ARG_0_:generate_stepped_item({
				name = _FORV_8_[1],
				text_id = _FORV_8_[1] .. "_text",
				help_id = _FORV_8_[1] .. "_help",
				callback = "hoxhud_set_option",
				min = _FORV_8_[3][1],
				max = _FORV_8_[3][2],
				step = _FORV_8_[3][3],
				show_value = true
			}))
		end
	end
	table.insert(_ARG_1_, _ARG_2_)
end
function HoxHudMenuGenerator.insert_hoxhud_options(_ARG_0_, _ARG_1_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_[1]) do
		if _FORV_7_.name and _FORV_7_.name == "options" then
			table.insert(_FORV_7_, #_FORV_7_ - 1, {
				name = "hoxhud_options",
				next_node = "hoxhud_options",
				_meta = "item",
				text_id = "menu_hoxhud_options",
				help_id = "menu_hoxhud_options_help"
			})
			break
		end
	end
	_ARG_0_:add_items_to_node(_ARG_1_[1], _ARG_0_:generate_node({
		topic_id = "menu_hoxhud_options",
		name = "hoxhud_options"
	}), managers.user.HOXHUD_SETTINGS)
end
getmetatable(PackageManager).script_data = function(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	if _ARG_0_.debug_script_data then
		_ARG_0_:debug_script_data(_ARG_1_, _ARG_2_, _ARG_2_ ~= Idstring("gamedata/hoxhud_credits") and _ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...))
	end
	if _ARG_1_ == Idstring("menu") and _ARG_2_ == Idstring("gamedata/menus/start_menu") then
		HoxHudMenuGenerator:insert_skill_profiler(_ARG_2_ ~= Idstring("gamedata/hoxhud_credits") and _ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...))
		HoxHudMenuGenerator:insert_inspect_player(_ARG_2_ ~= Idstring("gamedata/hoxhud_credits") and _ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...))
		HoxHudMenuGenerator:insert_hoxhud_options(_ARG_2_ ~= Idstring("gamedata/hoxhud_credits") and _ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...))
		HoxHudMenuGenerator:insert_hoxhud_credits(_ARG_2_ ~= Idstring("gamedata/hoxhud_credits") and _ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...))
	elseif _ARG_1_ == Idstring("menu") and _ARG_2_ == Idstring("gamedata/menus/pause_menu") then
		HoxHudMenuGenerator:insert_inspect_player(_ARG_2_ ~= Idstring("gamedata/hoxhud_credits") and _ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...))
		HoxHudMenuGenerator:insert_hoxhud_options(_ARG_2_ ~= Idstring("gamedata/hoxhud_credits") and _ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...))
	elseif _ARG_1_ == Idstring("credits") and _ARG_2_ == Idstring("gamedata/hoxhud_credits") then
		return {
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "fill",
					text = _ARG_0_
				}
			end)("HOXHUD CREDITS"),
			{_meta = "br"},
			{_meta = "br"},
			{
				src = "guis/textures/pd2/blackmarket/icons/characters/old_hoxton",
				_meta = "image"
			},
			{_meta = "br"},
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "fill",
					text = _ARG_0_
				}
			end)("THE HOXHUD CREW"),
			{_meta = "br"},
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Lead Developer"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] Olipro"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Lua API & SimpleInput/SimpleMenu/Skill Profiler Support"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Harfatus"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Game Code Analysis & RE"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] Olipro"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Harfatus"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("bar0th"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Lua Source Reconstruction"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("bar0th"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("HoxHud Logo & Graphic Design"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] Odinn"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("\"HoxHud Initialised\" Voiceover"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Pete Gold"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Community Support Officers"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] Odinn"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] Olipro"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] Corpsy"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] LastOfSpades"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] R.N.O"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Community Support Moderators"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("-WonderBolt- Spitfire"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Fleshpound Mercenary"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Special Thanks"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] Azarona"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Albrecht"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("GeneralMcBadass"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("OVERKILL_Almir"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("OVERKILL Software"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Team$Evil"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Comic Relief & Epic Classiness"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] =T$ECore= Caboose"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Eh Merkin"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Crew Support"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Corpsy's Mum ;-)"),
			{_meta = "br"},
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "fill",
					text = _ARG_0_
				}
			end)("HOXHUD LOCALISATIONS"),
			{_meta = "br"},
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Dutch Localisation"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("</Jebediah>"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] Odinn"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Finnish Localisation"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Wasaur"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("French Localisation"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("[TEC] R.N.O"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Bighacker"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Cosmik"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("German Localisation"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("=T$E= X.M.G"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("KleinerBaer"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Lavaground / []V[] []_[] []-[]"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Meerel"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Ryutaro Dojima"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Tschilkroete"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Hungarian Localisation"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("BlackD0127"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Indonesian Localisation"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("papin97"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Italian Localisation"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Oktober"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Portugese Localisation"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Dantger"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Russian Localisation"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("=T$ETag= SuperNova"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("\208\150\208\188\208\190\208\191\208\181\208\187\209\140"),
			{_meta = "br"},
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "title",
					text = _ARG_0_
				}
			end)("Spanish Localisation"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Tridan Dieno"),
			(function(_ARG_0_)
				return {
					_meta = "text",
					type = "name",
					text = _ARG_0_
				}
			end)("Hoxi Makara"),
			{_meta = "br"},
			{_meta = "br"}
		}
	end
	return _ARG_2_ ~= Idstring("gamedata/hoxhud_credits") and _ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...)
end
MenuHoxHudOptionInitiator = MenuHoxHudOptionInitiator or class()
function MenuHoxHudOptionInitiator.modify_node(_ARG_0_, _ARG_1_)
	if managers.user:setting_exists(_ARG_1_:parameters().name) then
		return managers.user:get_setting(_ARG_1_:parameters().name)
	end
	for _FORV_5_, _FORV_6_ in ipairs(_ARG_1_:items()) do
		if managers.user:setting_exists(_FORV_6_:name()) then
			if type((managers.user:get_setting(_FORV_6_:name()))) == "boolean" then
				_FORV_6_:set_value(managers.user:get_setting(_FORV_6_:name()) and "on" or "off")
			else
				_FORV_6_:set_value((managers.user:get_setting(_FORV_6_:name())))
			end
		end
	end
	return _ARG_1_
end
function MenuCallbackHandler.hoxhud_set_toggle(_ARG_0_, _ARG_1_)
	managers.user:set_setting(_ARG_1_:name(), _ARG_1_:value() == "on")
end
function MenuCallbackHandler.hoxhud_set_option(_ARG_0_, _ARG_1_)
	managers.user:set_setting(_ARG_1_:name(), _ARG_1_:value())
end
function MenuCallbackHandler.skill_profiler(_ARG_0_)
	managers.skilltree:profileMain()
end
function MenuCallbackHandler.inspect_player(_ARG_0_, _ARG_1_)
	Steam:overlay_activate("url", string.format(tweak_data.hoxhud.inspect_url, _ARG_1_:parameters().peer:user_id()))
	managers.menu:back(true)
end
InspectPlayer = InspectPlayer or class()
function InspectPlayer.modify_node(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	if managers.network:session() then
		for _FORV_8_, _FORV_9_ in pairs(managers.network:session():peers()) do
			if _FORV_9_ and _FORV_9_:rpc() and _FORV_9_:name() then
				if _ARG_3_ == "mute_player" then
					_ARG_1_:create_item(_ARG_3_ == "mute_player" and {
						type = "CoreMenuItemToggle.ItemToggle",
						{
							_meta = "option",
							icon = "guis/textures/menu_tickbox",
							value = "on",
							x = 24,
							y = 0,
							w = 24,
							h = 24,
							s_icon = "guis/textures/menu_tickbox",
							s_x = 24,
							s_y = 24,
							s_w = 24,
							s_h = 24
						},
						{
							_meta = "option",
							icon = "guis/textures/menu_tickbox",
							value = "off",
							x = 0,
							y = 0,
							w = 24,
							h = 24,
							s_icon = "guis/textures/menu_tickbox",
							s_x = 0,
							s_y = 24,
							s_w = 24,
							s_h = 24
						}
					}, {
						name = _FORV_9_:name(),
						text_id = _FORV_9_:name() .. " (" .. (_FORV_9_:rank() > 0 and managers.experience:rank_string(_FORV_9_:rank()) .. "-" or "") .. (_FORV_9_:level() or "") .. ")",
						callback = _ARG_3_ or "inspect_player",
						to_upper = false,
						localize = "false",
						rpc = _FORV_9_:rpc(),
						peer = _FORV_9_
					}):set_value(_FORV_9_:is_muted() and "on" or "off")
				end
				deep_clone(_ARG_1_):add_item((_ARG_1_:create_item(_ARG_3_ == "mute_player" and {
					type = "CoreMenuItemToggle.ItemToggle",
					{
						_meta = "option",
						icon = "guis/textures/menu_tickbox",
						value = "on",
						x = 24,
						y = 0,
						w = 24,
						h = 24,
						s_icon = "guis/textures/menu_tickbox",
						s_x = 24,
						s_y = 24,
						s_w = 24,
						s_h = 24
					},
					{
						_meta = "option",
						icon = "guis/textures/menu_tickbox",
						value = "off",
						x = 0,
						y = 0,
						w = 24,
						h = 24,
						s_icon = "guis/textures/menu_tickbox",
						s_x = 0,
						s_y = 24,
						s_w = 24,
						s_h = 24
					}
				}, {
					name = _FORV_9_:name(),
					text_id = _FORV_9_:name() .. " (" .. (_FORV_9_:rank() > 0 and managers.experience:rank_string(_FORV_9_:rank()) .. "-" or "") .. (_FORV_9_:level() or "") .. ")",
					callback = _ARG_3_ or "inspect_player",
					to_upper = false,
					localize = "false",
					rpc = _FORV_9_:rpc(),
					peer = _FORV_9_
				})))
			end
		end
	end
	managers.menu:add_back_button((deep_clone(_ARG_1_)))
	return (deep_clone(_ARG_1_))
end
function KickPlayer.modify_node(_ARG_0_, _ARG_1_, _ARG_2_)
	return InspectPlayer.modify_node(_ARG_0_, _ARG_1_, _ARG_2_, "kick_player")
end
function MutePlayer.modify_node(_ARG_0_, _ARG_1_, _ARG_2_)
	return InspectPlayer.modify_node(_ARG_0_, _ARG_1_, _ARG_2_, "mute_player")
end
function MenuManager.activate(_ARG_0_, ...)
	_ARG_0_:hh_upd_chk(function(...)
		_UPVALUE0_.hoxhud_http(_UPVALUE1_, ...)
	end)
	if not io.open("HoxHud/P1.6.tutorialseen", "r") then
		io.open("HoxHud/P1.6.tutorialseen", "w+"):close()
		SimpleMenu:new(managers.localization:text("hoxhud_first_launch_title"), managers.localization:text("hoxhud_first_launch_text"), {
			{
				text = managers.localization:text("dialog_yes"),
				is_focused_button = true,
				callback = function()
					Steam:overlay_activate("url", managers.localization:text("hoxhud_first_launch_url"))
				end
			},
			{
				text = managers.localization:text("dialog_no"),
				is_cancel_button = true
			}
		}):show()
	else
		io.open("HoxHud/P1.6.tutorialseen", "r"):close()
	end
	return _UPVALUE0_.activate(_ARG_0_, ...)
end
function MenuManager.open_menu(_ARG_0_, ...)
	_UPVALUE0_.open_menu(_ARG_0_, ...)
	if Global.needMoneyCheck and not tweak_data.hoxhud.disable_money_cheat_checker then
		pcall(game_state_machine.check_money_abuse, game_state_machine)
		Global.needMoneyCheck = nil
	end
end
