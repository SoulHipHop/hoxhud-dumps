function ECMJammerBase.init(_ARG_0_, ...)
	_UPVALUE0_.init(_ARG_0_, ...)
	_ARG_0_._in_tab_screen = tweak_data.hoxhud.tab_screen_timers.ecm_jammer or false
	_ARG_0_._hud_timer = _ARG_0_._hud_timer or managers.hud:add_hud_timer(_ARG_0_._in_tab_screen, tweak_data.hoxhud.ecm_name, {
		tweak_name = "ecm_jammer",
		timer_complete = tweak_data.hoxhud.ecm_expire_color,
		timer_flash = tweak_data.hoxhud.ecm_feedback_flash,
		text_color = tweak_data.hoxhud.ecm_text_color,
		text_flash = tweak_data.hoxhud.ecm_feedback_flash
	})
	if _ARG_0_._hud_timer then
		_ARG_0_._hud_timer:set_visible(false)
	end
end
function ECMJammerBase.set_active(_ARG_0_, _ARG_1_, ...)
	_ARG_0_:_do_upgrade_check_and_sync(_ARG_1_)
	_UPVALUE0_.set_active(_ARG_0_, _ARG_1_, ...)
	if _ARG_1_ and _ARG_0_._hud_timer then
		_ARG_0_._hud_timer:set_visible(true)
	end
end
function ECMJammerBase._set_feedback_active(_ARG_0_, _ARG_1_, ...)
	_ARG_1_ = _ARG_1_ or false
	if _ARG_1_ == _ARG_0_._feedback_active then
		return
	end
	_UPVALUE0_._set_feedback_active(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_._feedback_active then
		if _ARG_0_._hud_timer then
			_ARG_0_._hud_timer:set_name_text(tweak_data.hoxhud.ecm_feedback_name)
			_ARG_0_._hud_timer:set_jammed(true)
		end
		managers.hud:update_feedback_count(1)
	else
		if _ARG_0_._hud_timer then
			_ARG_0_._hud_timer:set_name_text(tweak_data.hoxhud.ecm_name)
			_ARG_0_._hud_timer:set_jammed(false)
		end
		managers.hud:update_feedback_count(-1)
	end
end
function ECMJammerBase.check_battery(_ARG_0_, ...)
	_UPVALUE0_.check_battery(_ARG_0_, ...)
	if tweak_data.hoxhud.ecm_low_alert and not _ARG_0_._played_alert and _ARG_0_._battery_life <= tweak_data.hoxhud.ecm_low_alert and io.open("HoxHud/ecm_alert.mp3", "r") then
		io.open("HoxHud/ecm_alert.mp3", "r"):close()
		_ARG_0_._played_alert = PlayMediaV2("HoxHud/ecm_alert.mp3")
		_ARG_0_._played_alert:play()
	end
	if _ARG_0_._battery_life <= 0 and _ARG_0_._hud_timer then
		managers.hud:del_hud_timer(_ARG_0_._in_tab_screen, _ARG_0_._hud_timer)
		_ARG_0_._hud_timer = nil
	elseif _ARG_0_._hud_timer then
		_ARG_0_._hud_timer:set_completion(_ARG_0_._battery_life, _ARG_0_._max_battery_life)
	end
end
function ECMJammerBase.destroy(_ARG_0_, ...)
	_UPVALUE0_.destroy(_ARG_0_, ...)
	if _ARG_0_._hud_timer then
		managers.hud:del_hud_timer(_ARG_0_._in_tab_screen, _ARG_0_._hud_timer)
		_ARG_0_._hud_timer = nil
	end
end
