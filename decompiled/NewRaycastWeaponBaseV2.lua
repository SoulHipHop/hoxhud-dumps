function NewRaycastWeaponBase.set_gadget_on(_ARG_0_, ...)
	_ARG_0_._last_gadget_state = {
		...
	}
	_UPVALUE0_.set_gadget_on(_ARG_0_, ...)
end
function NewRaycastWeaponBase.on_enabled(_ARG_0_, ...)
	_UPVALUE0_.on_enabled(_ARG_0_, ...)
	_ARG_0_:set_gadget_on(unpack(_ARG_0_._last_gadget_state or {false, true}))
end
function NewRaycastWeaponBase.on_disabled(_ARG_0_, ...)
	_ARG_0_.set_gadget_on = _UPVALUE0_.set_gadget_on
	_UPVALUE0_.on_disabled(_ARG_0_, ...)
	_ARG_0_.set_gadget_on = _ARG_0_.set_gadget_on
end
