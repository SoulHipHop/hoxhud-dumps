function NetworkGame._update_deployable_equipment_state(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	for _FORV_7_, _FORV_8_ in ipairs(World:find_units_quick("all", 14, 25, 26)) do
		if _FORV_8_:base() and _FORV_8_:base().server_information and _FORV_8_:base():server_information() and _FORV_8_:base():server_information().owner_peer_id == _ARG_1_ then
			_FORV_8_:base():server_information().owner_peer_id = _ARG_2_
			_FORV_8_:interaction():set_active(_ARG_3_, true)
			if (_FORV_8_:base().get_name_id and _FORV_8_:base():get_name_id() or "") == "trip_mine" then
				if _ARG_3_ then
					managers.network:session():peer(_ARG_2_):send_queued_sync("activate_trip_mine", _FORV_8_)
					managers.network:session():peer(_ARG_2_):send_queued_sync("sync_trip_mine_set_armed", _FORV_8_, _FORV_8_:base()._prev_armed_state, _FORV_8_:base()._length)
					_FORV_8_:base():set_armed(_FORV_8_:base()._prev_armed_state)
				else
					_FORV_8_:base()._prev_armed_state = _FORV_8_:base()._armed
					_FORV_8_:base()._first_armed = nil
					_FORV_8_:base()._sensor_upgrade = false
					_FORV_8_:base():set_armed(false)
					_FORV_8_:base()._sensor_upgrade = _FORV_8_:base()._sensor_upgrade
				end
			elseif (_FORV_8_:base().get_name_id and _FORV_8_:base():get_name_id() or "") == "sentry_gun" then
				_FORV_8_:brain()[_ARG_3_ and "switch_on" or "switch_off"](_FORV_8_:brain())
				if _ARG_3_ then
					managers.network:session():peer(_ARG_2_):send_queued_sync("from_server_sentry_gun_place_result", _ARG_2_, 0, _FORV_8_, _FORV_8_:base()._rot_speed_multiplier, _FORV_8_:base()._spread_multiplier, _FORV_8_:base()._has_shield)
				end
			end
		end
	end
end
function NetworkGame.on_peer_removed(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	if not _ARG_0_._members[_ARG_2_] then
		return
	end
	if Network:multiplayer() and Network:is_server() and Global.game_settings.permission ~= "public" and _ARG_3_ ~= "kicked" then
		_ARG_0_:_update_deployable_equipment_state(_ARG_2_, _ARG_1_:user_id(), false)
	end
	return _UPVALUE0_.on_peer_removed(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
end
function NetworkGame.spawn_dropin_player(_ARG_0_, _ARG_1_, ...)
	_UPVALUE0_.spawn_dropin_player(_ARG_0_, _ARG_1_, ...)
	if Network:multiplayer() and Network:is_server() and Global.game_settings.permission ~= "public" then
		_ARG_0_:_update_deployable_equipment_state(managers.network:session():peer(_ARG_1_):user_id(), managers.network:session():peer(_ARG_1_):id(), true)
	end
end
