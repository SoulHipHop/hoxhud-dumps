if not Application:IdToColor(Application:ColorToId(Steam:userid(), Application:ColAlloc())) then
	return
end
SkillTreeManager.PROFILE_OPERATIONS = {
	LOAD = 1,
	SAVE = 2,
	ADD = 3,
	DEL = 4,
	RENAME = 5,
	SWAP = 6,
	SWAP2 = 7
}
function SkillTreeManager.init(_ARG_0_, ...)
	_ARG_0_._switchProfilesForFree = tweak_data.hoxhud.debug_switch_skillprofiles_for_free or false
	_ARG_0_._totalSkillProfiles = tweak_data.hoxhud.total_skill_profiles or 5
	_ARG_0_._profileFile = (string.len("HoxHud") > 0 and "HoxHud" .. "\\" or "") .. tostring(Steam:userid()) .. "-skilltrees.json"
	_ARG_0_._strings = tweak_data.hoxhud.local_strings
	return _UPVALUE0_.init(_ARG_0_, ...)
end
function SkillTreeManager.repairProfiles(_ARG_0_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._skillProfiles) do
		table.insert({
			[_FORV_6_] = _FORV_7_
		}, _FORV_7_)
	end
	for _FORV_6_, _FORV_7_ in pairs(_ARG_0_._skillProfiles) do
		if tonumber(_FORV_6_) and not {
			[_FORV_6_] = true
		}[_FORV_6_] then
			table.insert({
				[_FORV_6_] = _FORV_7_
			}, _FORV_7_)
		elseif not {
			[_FORV_6_] = true
		}[_FORV_6_] then
		end
	end
	while #{
		[_FORV_6_] = _FORV_7_
	} > #{
		[_FORV_6_] = _FORV_7_
	}.names do
		table.remove({
			[_FORV_6_] = _FORV_7_
		}, #{
			[_FORV_6_] = _FORV_7_
		})
	end
	_ARG_0_._skillProfiles = {
		[_FORV_6_] = _FORV_7_
	}
	_ARG_0_._skillProfiles.version = 2
	_ARG_0_:doSave()
end
function SkillTreeManager.upgradeToVersion3(_ARG_0_)
	_ARG_0_._skillProfiles.specializations = {}
	for _FORV_4_, _FORV_5_ in ipairs(_ARG_0_._skillProfiles) do
		_ARG_0_._skillProfiles.specializations[_FORV_4_] = 0
	end
	_ARG_0_._skillProfiles.version = 3
end
function SkillTreeManager.upgradeToVersion4(_ARG_0_)
	_ARG_0_._skillProfiles.loadout_slots = {}
	_ARG_0_._skillProfiles.version = 4
end
function SkillTreeManager.upgradeToVersion5(_ARG_0_)
	_ARG_0_._skillProfiles.extra_data = {}
	_ARG_0_._skillProfiles.version = 5
end
function SkillTreeManager.profileInit(_ARG_0_)
	if not SteamAPI:FileExists("skilltrees.json") and io.open(_ARG_0_._profileFile, "r") then
		SteamAPI:FileWrite("skilltrees.json", io.open(_ARG_0_._profileFile, "r"):read("*all"))
		io.open(_ARG_0_._profileFile, "r"):close()
		os.remove(_ARG_0_._profileFile)
	end
	if SteamAPI:FileExists("skilltrees.json") then
		_ARG_0_._skillProfiles = JSON:decode(SteamAPI:FileRead("skilltrees.json"))
		if type(_ARG_0_._skillProfiles) ~= "table" then
			print("Warning: JSON was found to be invalid, reiniting profiles")
			_ARG_0_._skillProfiles = nil
		else
			if not _ARG_0_._skillProfiles.version then
				_ARG_0_:repairProfiles()
			end
			if _ARG_0_._skillProfiles.version == 1 then
				_ARG_0_:upgradeToVersion2()
			end
			if _ARG_0_._skillProfiles.version == 2 then
				_ARG_0_:upgradeToVersion3()
			end
			if _ARG_0_._skillProfiles.version == 3 then
				_ARG_0_:upgradeToVersion4()
			end
			if _ARG_0_._skillProfiles.version == 4 then
				_ARG_0_:upgradeToVersion5()
			end
		end
	end
	if not _ARG_0_._skillProfiles or #_ARG_0_._skillProfiles.names == 0 then
		print("Initializing all profiles with your current skill tree\n")
		_ARG_0_._skillProfiles = {
			names = {},
			specializations = {},
			loadout_slots = {},
			extra_data = {},
			version = 5
		}
		for _FORV_4_ = 1, _ARG_0_._totalSkillProfiles do
			_ARG_0_._skillProfiles[_FORV_4_] = deep_clone(_ARG_0_._global.skills)
			_ARG_0_._skillProfiles.names[_FORV_4_] = string.format(_ARG_0_._strings.default_profile_name, _FORV_4_)
			_ARG_0_._skillProfiles.specializations[_FORV_4_] = 0
		end
	end
	_ARG_0_._bootupProfile = _FOR_ or deep_clone(_ARG_0_._global.skills)
	_ARG_0_._bootupSpecialization = _ARG_0_:digest_value(_ARG_0_._global.specializations.current_specialization, false) or 0
	_ARG_0_._bootupLoadout = {
		managers.blackmarket:equipped_weapon_slot("primaries"),
		managers.blackmarket:equipped_weapon_slot("secondaries"),
		managers.blackmarket:equipped_melee_weapon_slot(),
		managers.blackmarket:equipped_armor_slot(),
		Global.player_manager.kit.equipment_slots[1]
	}
	_ARG_0_._treeSkillFinder = {}
	for _FORV_4_, _FORV_5_ in pairs(tweak_data.skilltree.trees) do
		for _FORV_9_, _FORV_10_ in pairs(_FORV_5_.tiers) do
			for _FORV_14_, _FORV_15_ in pairs(_FORV_10_) do
				_ARG_0_._treeSkillFinder[_FORV_15_] = _FORV_4_
			end
		end
	end
	_ARG_0_._profilerInited = true
end
function SkillTreeManager.profileMain(_ARG_0_)
	if not _ARG_0_._profilerInited then
		_ARG_0_:profileInit()
	end
	SimpleMenuV2:new(_ARG_0_._strings.skillprofiler_title, _ARG_0_._strings.skillprofiler_text, {
		{
			text = _ARG_0_._strings.profile_ops[_ARG_0_.PROFILE_OPERATIONS.LOAD],
			callback = _ARG_0_.profileChange,
			data = _ARG_0_.PROFILE_OPERATIONS.LOAD,
			Class = _ARG_0_
		},
		{
			text = _ARG_0_._strings.profile_ops[_ARG_0_.PROFILE_OPERATIONS.SAVE],
			callback = _ARG_0_.profileChange,
			data = _ARG_0_.PROFILE_OPERATIONS.SAVE,
			Class = _ARG_0_
		},
		{
			text = _ARG_0_._strings.profile_ops[_ARG_0_.PROFILE_OPERATIONS.RENAME],
			callback = _ARG_0_.profileChange,
			data = _ARG_0_.PROFILE_OPERATIONS.RENAME,
			Class = _ARG_0_
		},
		{
			text = _ARG_0_._strings.profile_ops[_ARG_0_.PROFILE_OPERATIONS.ADD],
			callback = _ARG_0_.newProfile,
			data = _ARG_0_.PROFILE_OPERATIONS.ADD,
			Class = _ARG_0_
		},
		{
			text = _ARG_0_._strings.profile_ops[_ARG_0_.PROFILE_OPERATIONS.DEL],
			callback = _ARG_0_.profileChange,
			data = _ARG_0_.PROFILE_OPERATIONS.DEL,
			Class = _ARG_0_
		},
		{
			text = _ARG_0_._strings.profile_ops[_ARG_0_.PROFILE_OPERATIONS.SWAP],
			callback = _ARG_0_.profileChange,
			data = _ARG_0_.PROFILE_OPERATIONS.SWAP,
			Class = _ARG_0_
		},
		{
			text = managers.localization:text("dialog_cancel"),
			is_cancel_button = true
		}
	}):show()
end
function SkillTreeManager.profileChange(_ARG_0_, _ARG_1_)
	if _ARG_1_ == _ARG_0_.PROFILE_OPERATIONS.LOAD then
	else
	end
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._skillProfiles.names) do
		_FORV_7_ = _FORV_7_ .. (0 < (_ARG_0_._skillProfiles.specializations[_FORV_6_] or 0) and " - " .. managers.localization:text(tweak_data.skilltree.specializations[_ARG_0_._skillProfiles.specializations[_FORV_6_] or 0].name_id) or "")
		table.insert({
			{
				text = _ARG_0_._strings.bootup_profile_name .. " - " .. managers.localization:text(tweak_data.skilltree.specializations[_ARG_0_._bootupSpecialization].name_id),
				callback = managers.skilltree.doProfileOp,
				data = {profileID = -1, action = _ARG_1_},
				Class = _ARG_0_
			}
		} or {}, {
			text = _FORV_7_,
			callback = managers.skilltree.doProfileOp,
			data = {profileID = _FORV_6_, action = _ARG_1_},
			Class = managers.skilltree
		})
	end
	table.insert({
		{
			text = _ARG_0_._strings.bootup_profile_name .. " - " .. managers.localization:text(tweak_data.skilltree.specializations[_ARG_0_._bootupSpecialization].name_id),
			callback = managers.skilltree.doProfileOp,
			data = {profileID = -1, action = _ARG_1_},
			Class = _ARG_0_
		}
	} or {}, {
		text = managers.localization:text("dialog_cancel"),
		is_cancel_button = true,
		callback = _ARG_0_.profileMain,
		Class = _ARG_0_
	})
	SimpleMenuV2:new(_ARG_0_._strings.skillprofiler_title, string.format(_ARG_0_._strings.skillprofiler_op_text, _ARG_0_._strings.profile_ops[_ARG_1_]), {
		{
			text = _ARG_0_._strings.bootup_profile_name .. " - " .. managers.localization:text(tweak_data.skilltree.specializations[_ARG_0_._bootupSpecialization].name_id),
			callback = managers.skilltree.doProfileOp,
			data = {profileID = -1, action = _ARG_1_},
			Class = _ARG_0_
		}
	} or {}):show()
end
function SkillTreeManager.newProfile(_ARG_0_)
	_ARG_0_._skillProfiles[#_ARG_0_._skillProfiles.names + 1] = deep_clone(_ARG_0_._global.skills)
	_ARG_0_._skillProfiles.names[#_ARG_0_._skillProfiles.names + 1] = string.format(_ARG_0_._strings.default_profile_name, #_ARG_0_._skillProfiles.names + 1)
	_ARG_0_._skillProfiles.specializations[#_ARG_0_._skillProfiles.names + 1] = _ARG_0_:digest_value(_ARG_0_._global.specializations.current_specialization, false) or 0
	_ARG_0_:doSave()
	_ARG_0_:profileMain()
end
function SkillTreeManager.doProfileOp(_ARG_0_, _ARG_1_)
	if _ARG_1_.action == _ARG_0_.PROFILE_OPERATIONS.SAVE then
		_ARG_0_:saveProfile(_ARG_1_.profileID)
	elseif _ARG_1_.action == _ARG_0_.PROFILE_OPERATIONS.LOAD then
		if _ARG_1_.profileID ~= -1 or not _ARG_0_._bootupProfile then
		end
		if _ARG_1_.profileID ~= -1 or not _ARG_0_._bootupSpecialization then
		end
		if _ARG_1_.profileID ~= -1 or not _ARG_0_._bootupLoadout then
		end
		if not tweak_data.hoxhud.skip_respec_cost_warning and not _ARG_0_:_is_tree_identical_to_equipped(_ARG_0_._skillProfiles[_ARG_1_.profileID]) and not _ARG_0_:_get_matching_switch(_ARG_0_._skillProfiles[_ARG_1_.profileID]) then
			SimpleMenuV2:new(_ARG_0_._strings.skillprofiler_title, string.format(_ARG_0_:get_tree_switch_cost(_ARG_0_._skillProfiles[_ARG_1_.profileID]) > 0 and _ARG_0_._strings.respec_warn_text or _ARG_0_._strings.respec_cheaper_text, managers.localization:text("cash_sign"), managers.money:add_decimal_marks_to_string(tostring(math.abs((_ARG_0_:get_tree_switch_cost(_ARG_0_._skillProfiles[_ARG_1_.profileID])))))), {
				{
					text = managers.localization:text("dialog_yes"),
					callback = function()
						_UPVALUE0_:loadProfile(_UPVALUE1_, _UPVALUE2_, _UPVALUE3_)
					end
				},
				{
					text = managers.localization:text("dialog_no"),
					is_cancel_button = true
				}
			}):show()
		else
			_ARG_0_:loadProfile(_ARG_0_._skillProfiles[_ARG_1_.profileID], _ARG_0_._skillProfiles.specializations[_ARG_1_.profileID], _ARG_0_._skillProfiles.loadout_slots[_ARG_1_.profileID])
		end
	elseif _ARG_1_.action == _ARG_0_.PROFILE_OPERATIONS.RENAME then
		_ARG_0_._rename_input = SimpleInput:new({
			intro = string.format(_ARG_0_._strings.rename_text, _ARG_0_._skillProfiles.names[_ARG_1_.profileID]),
			intro_color = Color.green,
			prompt = _ARG_0_._strings.rename_prompt,
			cblk = _ARG_0_.renameProfile,
			data = {
				self = _ARG_0_,
				profileID = _ARG_1_.profileID
			},
			cblkself = _ARG_0_
		})
		_ARG_0_._rename_input:show()
	elseif _ARG_1_.action == _ARG_0_.PROFILE_OPERATIONS.DEL then
		SimpleMenuV2:new(_ARG_0_._strings.skillprofiler_title, string.format(_ARG_0_._strings.skillprofiler_del_text, _ARG_0_._skillProfiles.names[_ARG_1_.profileID]), {
			{
				text = managers.localization:text("dialog_yes"),
				callback = function()
					table.remove(_UPVALUE0_._skillProfiles, _UPVALUE1_.profileID)
					table.remove(_UPVALUE0_._skillProfiles.names, _UPVALUE1_.profileID)
					table.remove(_UPVALUE0_._skillProfiles.specializations, _UPVALUE1_.profileID)
					_UPVALUE0_:doSave()
					_UPVALUE0_:profileMain()
				end
			},
			{
				text = managers.localization:text("dialog_no"),
				is_cancel_button = true
			}
		}):show()
	elseif _ARG_1_.action == _ARG_0_.PROFILE_OPERATIONS.SWAP then
		_ARG_0_._swapStageOne = _ARG_1_.profileID
		_ARG_0_:profileChange(_ARG_0_.PROFILE_OPERATIONS.SWAP2)
	elseif _ARG_1_.action == _ARG_0_.PROFILE_OPERATIONS.SWAP2 then
		_ARG_0_:swapProfiles(_ARG_0_._swapStageOne, _ARG_1_.profileID)
		_ARG_0_._swapStageOne = nil
	end
end
function SkillTreeManager.doSave(_ARG_0_)
	_ARG_0_._skillProfiles.extra_data = {
		rank = managers.experience:current_rank(),
		level = managers.experience:current_level()
	}
	SteamAPI:FileWrite("skilltrees.json", JSON:encode_pretty(_ARG_0_._skillProfiles))
end
function SkillTreeManager.renameProfile(_ARG_0_, _ARG_1_, _ARG_2_)
	if not _ARG_1_ or _ARG_1_ == "" then
		return
	end
	_ARG_2_.self._skillProfiles.names[_ARG_2_.profileID] = _ARG_1_
	_ARG_2_.self:doSave()
	_ARG_2_.self._rename_input:hide()
	_ARG_2_.self:profileMain()
end
function SkillTreeManager.saveProfile(_ARG_0_, _ARG_1_)
	_ARG_0_._skillProfiles[_ARG_1_] = deep_clone(_ARG_0_._global.skills)
	_ARG_0_._skillProfiles.specializations[_ARG_1_] = _ARG_0_:digest_value(_ARG_0_._global.specializations.current_specialization, false) or 0
	_ARG_0_._skillProfiles.loadout_slots[_ARG_1_] = {
		managers.blackmarket:equipped_weapon_slot("primaries"),
		managers.blackmarket:equipped_weapon_slot("secondaries"),
		managers.blackmarket:equipped_melee_weapon_slot(),
		managers.blackmarket:equipped_armor_slot(),
		Global.player_manager.kit.equipment_slots[1]
	}
	_ARG_0_:doSave()
	_ARG_0_:profileMain()
end
function SkillTreeManager.swapProfiles(_ARG_0_, _ARG_1_, _ARG_2_)
	_ARG_0_._skillProfiles[_ARG_1_] = _ARG_0_._skillProfiles[_ARG_2_]
	_ARG_0_._skillProfiles[_ARG_2_] = _ARG_0_._skillProfiles[_ARG_1_]
	_ARG_0_._skillProfiles.names[_ARG_1_] = _ARG_0_._skillProfiles.names[_ARG_2_]
	_ARG_0_._skillProfiles.names[_ARG_2_] = _ARG_0_._skillProfiles.names[_ARG_1_]
	_ARG_0_._skillProfiles.specializations[_ARG_1_] = _ARG_0_._skillProfiles.specializations[_ARG_2_]
	_ARG_0_._skillProfiles.specializations[_ARG_2_] = _ARG_0_._skillProfiles.specializations[_ARG_1_]
	_ARG_0_._skillProfiles.loadout_slots[_ARG_1_] = _ARG_0_._skillProfiles.loadout_slots[_ARG_2_]
	_ARG_0_._skillProfiles.loadout_slots[_ARG_2_] = _ARG_0_._skillProfiles.loadout_slots[_ARG_1_]
	_ARG_0_:doSave()
	_ARG_0_:profileMain()
end
function SkillTreeManager.calc_skillpoint_cost(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_)
	for _FORV_9_, _FORV_10_ in ipairs(_ARG_3_.tiers) do
		if table.contains(_FORV_10_, _ARG_2_) then
			break
		end
	end
	return managers.money:get_skillpoint_cost(_ARG_1_, _FORV_9_, _ARG_0_:get_skill_points(_ARG_2_, _ARG_4_))
end
function SkillTreeManager.get_tree_switch_cost(_ARG_0_, _ARG_1_)
	for _FORV_6_ = 1, #tweak_data.skilltree.trees do
	end
	for _FORV_6_, _FORV_7_ in pairs(tweak_data.skilltree.trees) do
		if _ARG_1_[_FORV_7_.skill] and 0 < _ARG_1_[_FORV_7_.skill].unlocked then
			for _FORV_11_, _FORV_12_ in pairs(_FORV_7_.tiers) do
				for _FORV_16_, _FORV_17_ in pairs(_FORV_12_) do
					for _FORV_21_ = 1, _ARG_1_[_FORV_17_].unlocked do
					end
				end
			end
		end
	end
	return 0 - managers.money:get_skilltree_tree_respec_cost(_FORV_6_, false) + managers.money:get_skillpoint_cost(_FORV_7_, nil, _ARG_0_:get_skill_points(_FORV_7_.skill, _ARG_0_:next_skill_step(_FORV_7_.skill))) + _ARG_0_:calc_skillpoint_cost(_FORV_6_, _FORV_17_, _FORV_7_, _FORV_21_)
end
function SkillTreeManager.loadProfile(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	if _ARG_0_._switchProfilesForFree then
		function managers.money.on_respec_skilltree()
		end
		function managers.money.on_skillpoint_spent()
		end
	end
	function managers.statistics.publish_skills_to_steam()
	end
	if not _ARG_0_:_is_tree_identical_to_equipped(_ARG_1_) then
		if _ARG_0_:_get_matching_switch(_ARG_1_) then
			_ARG_0_:switch_skills((_ARG_0_:_get_matching_switch(_ARG_1_)))
		else
			_ARG_0_:manual_switch_skills(_ARG_1_)
		end
	end
	_ARG_0_:set_current_specialization(_ARG_2_)
	_ARG_0_:_equip_loadout_items(_ARG_3_)
	managers.money.on_respec_skilltree = managers.money.on_respec_skilltree
	managers.money.on_skillpoint_spend = managers.money.on_skillpoint_spent
	managers.statistics.publish_skills_to_steam = managers.statistics.publish_skills_to_steam
	if SystemInfo:platform() == Idstring("WIN32") then
		managers.statistics:publish_skills_to_steam()
	end
	managers.menu_component:refresh_player_profile_gui()
	if managers.menu_component._skilltree_gui then
		managers.menu_component:close_skilltree_gui()
		managers.menu_component:_create_skilltree_gui()
	end
end
function SkillTreeManager._equip_loadout_items(_ARG_0_, _ARG_1_)
	if not _ARG_1_ then
		return
	end
	if managers.blackmarket:get_crafted_category_slot("primaries", _ARG_1_[1]) and (function(_ARG_0_)
		return managers.blackmarket:weapon_unlocked(_ARG_0_) and managers.experience:current_level() >= managers.blackmarket:weapon_level(_ARG_0_)
	end)(managers.blackmarket:get_crafted_category_slot("primaries", _ARG_1_[1]).weapon_id) then
		managers.blackmarket:equip_weapon("primaries", _ARG_1_[1])
	end
	if managers.blackmarket:get_crafted_category_slot("secondaries", _ARG_1_[2]) and (function(_ARG_0_)
		return managers.blackmarket:weapon_unlocked(_ARG_0_) and managers.experience:current_level() >= managers.blackmarket:weapon_level(_ARG_0_)
	end)(managers.blackmarket:get_crafted_category_slot("secondaries", _ARG_1_[2]).weapon_id) then
		managers.blackmarket:equip_weapon("secondaries", _ARG_1_[2])
	end
	if Global.blackmarket_manager.melee_weapons[_ARG_1_[3]] and Global.blackmarket_manager.melee_weapons[_ARG_1_[3]].unlocked and managers.experience:current_level() >= Global.blackmarket_manager.melee_weapons[_ARG_1_[3]].level then
		managers.blackmarket:equip_melee_weapon(_ARG_1_[3])
	end
	if Global.blackmarket_manager.armors[_ARG_1_[4]] then
		for _FORV_12_, _FORV_13_ in pairs(tweak_data.upgrades.level_tree) do
			if _FORV_13_.upgrades then
				for _FORV_17_, _FORV_18_ in ipairs(_FORV_13_.upgrades) do
				end
			end
		end
		if Global.blackmarket_manager.armors[_ARG_1_[4]].unlocked and (not {
			[tweak_data.upgrades.definitions[_FORV_18_].armor_id] = _FORV_12_
		}[_ARG_1_[4]] or managers.experience:current_level() >= {
			[tweak_data.upgrades.definitions[_FORV_18_].armor_id] = _FORV_12_
		}[_ARG_1_[4]]) then
			managers.blackmarket:equip_armor(_ARG_1_[4])
		end
	end
	if table.contains(managers.player:availible_equipment(1), _ARG_1_[5]) then
		managers.blackmarket:equip_deployable(_ARG_1_[5])
	end
end
function SkillTreeManager._is_tree_identical_to_equipped(_ARG_0_, _ARG_1_)
	for _FORV_5_, _FORV_6_ in pairs(_ARG_0_._global.skills) do
		if _ARG_1_[_FORV_5_] and _ARG_1_[_FORV_5_].unlocked ~= _FORV_6_.unlocked then
			return false
		end
	end
	return true
end
function SkillTreeManager._get_matching_switch(_ARG_0_, _ARG_1_)
	for _FORV_5_, _FORV_6_ in ipairs(_ARG_0_._global.skill_switches) do
		for _FORV_11_, _FORV_12_ in pairs(_FORV_6_.skills) do
			if _ARG_1_[_FORV_11_] and _ARG_1_[_FORV_11_].unlocked ~= _FORV_12_.unlocked then
				break
			end
		end
		if not true then
			return _FORV_5_
		end
	end
	return false
end
function SkillTreeManager.manual_switch_skills(_ARG_0_, _ARG_1_)
	for _FORV_5_ = 1, #tweak_data.skilltree.trees do
		if _ARG_0_:tree_unlocked(_FORV_5_) then
			_ARG_0_:on_respec_tree(_FORV_5_, false)
		end
	end
	for _FORV_5_, _FORV_6_ in ipairs(tweak_data.skilltree.trees) do
		if _ARG_1_[_FORV_6_.skill] and _ARG_1_[_FORV_6_.skill].unlocked > 0 then
			_ARG_0_:unlock_tree(_FORV_5_)
			for _FORV_10_, _FORV_11_ in ipairs(_FORV_6_.tiers) do
				if _ARG_0_:tier_unlocked(_FORV_5_, _FORV_10_) then
					for _FORV_15_, _FORV_16_ in ipairs(_FORV_11_) do
						for _FORV_20_ = 1, _ARG_1_[_FORV_16_].unlocked do
							if managers.money:can_afford_spend_skillpoint(_FORV_5_, _FORV_10_, _ARG_0_:get_skill_points(_FORV_16_, _ARG_0_:next_skill_step(_FORV_16_))) then
								_ARG_0_:unlock(_FORV_5_, _FORV_16_)
							end
						end
					end
				end
			end
		end
	end
end
if not SimpleMenuV2 then
	SimpleMenuV2 = class()
	function SimpleMenuV2.init(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
		_ARG_0_.dialog_data = {
			title = _ARG_1_,
			text = _ARG_2_,
			button_list = {},
			id = tostring(math.random(0, 4.2949673E9))
		}
		_ARG_0_.visible = false
		for _FORV_7_, _FORV_8_ in ipairs(_ARG_3_) do
			_FORV_8_.data = _FORV_8_.data or nil
			_FORV_8_.callback = _FORV_8_.callback or nil
			if _FORV_8_.is_focused_button then
				_ARG_0_.dialog_data.focus_button = #_ARG_0_.dialog_data.button_list + 1
			end
			table.insert(_ARG_0_.dialog_data.button_list, {
				text = _FORV_8_.text,
				callback_func = callback(_ARG_0_, _ARG_0_, "_do_callback", {
					data = _FORV_8_.data,
					callback = _FORV_8_.callback,
					Class = _FORV_8_.Class or nil
				}),
				cancel_button = _FORV_8_.is_cancel_button or false
			})
		end
		managers.system_menu.DIALOG_CLASS.update_input = _ARG_0_.patched_update_input
		managers.system_menu.GENERIC_DIALOG_CLASS.update_input = _ARG_0_.patched_update_input
		return _ARG_0_
	end
	function SimpleMenuV2._do_callback(_ARG_0_, _ARG_1_)
		if _ARG_1_.callback then
			if _ARG_1_.data then
				if _ARG_1_.Class then
					_ARG_1_.callback(_ARG_1_.Class, _ARG_1_.data)
				else
					_ARG_1_.callback(_ARG_1_.data)
				end
			elseif _ARG_1_.Class then
				_ARG_1_.callback(_ARG_1_.Class)
			else
				_ARG_1_.callback()
			end
		end
		_ARG_0_.visible = false
	end
	function SimpleMenuV2.show(_ARG_0_)
		if _ARG_0_.visible then
			return
		end
		_ARG_0_.visible = true
		managers.system_menu:show(_ARG_0_.dialog_data)
	end
	function SimpleMenuV2.hide(_ARG_0_)
		if _ARG_0_.visible then
			managers.system_menu:close(_ARG_0_.dialog_data.id)
			_ARG_0_.visible = false
			return
		end
	end
	function SimpleMenuV2.patched_update_input(_ARG_0_, _ARG_1_, _ARG_2_)
		if _ARG_0_._data.no_buttons then
			return
		end
		if _ARG_0_._controller:get_input_bool("menu_down") then
		elseif _ARG_0_._controller:get_input_bool("menu_up") then
		end
		if -1 == nil then
			if _ARG_0_._controller:get_input_axis("menu_move").y > _ARG_0_.MOVE_AXIS_LIMIT then
			else
			end
		end
		if -1 ~= nil then
			if _ARG_0_._move_button_dir == -1 and _ARG_0_._move_button_time and _ARG_1_ < _ARG_0_._move_button_time + _ARG_0_.MOVE_AXIS_DELAY then
			else
				_ARG_0_._panel_script:change_focus_button(-1)
			end
		end
		_ARG_0_._move_button_time, _ARG_0_._move_button_dir = _ARG_1_, -1
		if _ARG_0_._controller:get_input_axis("menu_scroll").y > _ARG_0_.MOVE_AXIS_LIMIT then
			_ARG_0_._panel_script:scroll_up()
		elseif _ARG_0_._controller:get_input_axis("menu_scroll").y < -_ARG_0_.MOVE_AXIS_LIMIT then
			_ARG_0_._panel_script:scroll_down()
		end
	end
end
if not SimpleInput then
	SimpleInput = SimpleInput or class()
	SimpleInput._visible = SimpleInput._visible or false
	function SimpleInput.init(_ARG_0_, _ARG_1_)
		_ARG_0_.cblk = _ARG_1_.cblk or function(...)
		end
		_ARG_0_.data = _ARG_1_.data or nil
		_ARG_0_.intro = _ARG_1_.intro
		_ARG_0_.cblkself = _ARG_1_.cblkself
		_ARG_0_.intro_color = _ARG_1_.intro_color or Color.white
		_ARG_0_.prompt = _ARG_1_.prompt
		_ARG_0_.isVisible = false
		_ARG_0_.chatWasVisible = false
		function managers.menu_component.force_create_chat(_ARG_0_)
			_ARG_0_._lobby_chat_gui_active = true
			if _ARG_0_._game_chat_gui then
				_ARG_0_:show_game_chat_gui()
				return
			end
			_ARG_0_:add_game_chat()
		end
		ChatGui.__update_caret = ChatGui.__update_caret or ChatGui.update_caret
		function ChatGui.update_caret(_ARG_0_, ...)
			if alive(_ARG_0_._input_panel) then
				return _ARG_0_:__update_caret(...)
			end
		end
		ControllerWrapper.__get_input_pressed = ControllerWrapper.__get_input_pressed or ControllerWrapper.get_input_pressed
		function ControllerWrapper.get_input_pressed(_ARG_0_, ...)
			if not SimpleInput._needIgnoreFirstPress then
				return _ARG_0_:__get_input_pressed(...)
			end
			SimpleInput._needIgnoreFirstPress = nil
			return false
		end
		SimpleInput._origFuncs = SimpleInput._origFuncs or {
			managers_chat_send_message = managers.chat.send_message,
			ChatGui_create_input_panel = ChatGui._create_input_panel,
			ChatGui_update_caret = ChatGui.update_caret,
			ChatGui_enter_key_callback = ChatGui.enter_key_callback,
			managers_menucomp_key_press_controller_support = managers.menu_component.key_press_controller_support,
			managers_hudchat_on_focus = managers.hud and managers.hud._hud_chat._on_focus or nil,
			managers_hudchat_receive_message = managers.hud and managers.hud._hud_chat.receive_message or nil
		}
	end
	function SimpleInput._remove_hooks(_ARG_0_)
		managers.menu_component:close_chat_gui()
		managers.chat.send_message = SimpleInput._origFuncs.managers_chat_send_message
		ChatGui._create_input_panel = SimpleInput._origFuncs.ChatGui_create_input_panel
		ChatGui.update_caret = SimpleInput._origFuncs.ChatGui_update_caret
		ChatGui.enter_key_callback = SimpleInput._origFuncs.ChatGui_enter_key_callback
		SimpleInput._needIgnoreFirstPress = true
		managers.menu_component.key_press_controller_support = SimpleInput._origFuncs.managers_menucomp_key_press_controller_support
		if _ARG_0_.chatWasVisible then
			_ARG_0_.chatWasVisible = false
			if managers.hud then
				managers.hud._hud_chat._on_focus = SimpleInput._origFuncs.managers_hudchat_on_focus
				managers.hud._hud_chat.receive_message = SimpleInput._origFuncs.managers_hudchat_receive_message
				managers.hud:set_chat_focus(false)
			else
				managers.menu_component:_create_lobby_chat_gui()
			end
		end
	end
	function SimpleInput._inject_hooks(_ARG_0_)
		if managers.menu_component._game_chat_gui or managers.hud and managers.hud._hud_chat then
			_ARG_0_.chatWasVisible = true
		end
		pcall(managers.menu_component.close_chat_gui, managers.menu_component)
		managers.chat.send_message = managers.chat.receive_message_by_name
		ChatGui._create_input_panel = _ARG_0_._create_input_panel
		ChatGui.update_caret = _ARG_0_._update_caret
		ChatGui.enter_key_callback = _ARG_0_._enter_key_callback
		function managers.menu_component.key_press_controller_support(...)
		end
		managers.SimpleInput = _ARG_0_
		managers.menu_component:force_create_chat()
		managers.menu_component._game_chat_gui.receive_message = _ARG_0_._receive_message
		if managers.hud then
			function managers.hud._hud_chat.receive_message(...)
			end
			function managers.hud._hud_chat._on_focus(...)
			end
		end
		managers.menu_component._game_chat_gui:open_page()
	end
	function SimpleInput.show(_ARG_0_)
		if SimpleInput._visible then
			return
		end
		SimpleInput._visible = true
		_ARG_0_.isVisible = true
		_ARG_0_:_inject_hooks()
		if _ARG_0_.intro then
			function managers.SimpleInput.cblk(...)
				return {
					msg = _UPVALUE0_.intro,
					color = _UPVALUE0_.intro_color
				}
			end
			managers.menu_component._game_chat_gui:receive_message(nil, nil, nil, nil)
			managers.SimpleInput.cblk = _ARG_0_.cblk
		end
	end
	function SimpleInput.hide(_ARG_0_)
		if not SimpleInput._visible then
			return
		end
		SimpleInput._visible = false
		_ARG_0_._needHide = false
		_ARG_0_.isVisible = false
		_ARG_0_:_remove_hooks()
	end
	function SimpleInput.is_visible(_ARG_0_)
		return _ARG_0_.isVisible
	end
	function SimpleInput._update_caret(_ARG_0_)
		if (not SimpleInput or SimpleInput._visible) and alive(_ARG_0_._input_panel) then
			return SimpleInput._origFuncs.ChatGui_update_caret(_ARG_0_)
		end
	end
	function SimpleInput._enter_key_callback(_ARG_0_)
		if not _ARG_0_._enabled then
			return
		end
		if Idstring((_ARG_0_._input_panel:child("input_text"):text())) == Idstring("/ready") then
			managers.menu_component:on_ready_pressed_mission_briefing_gui()
		elseif string.len((_ARG_0_._input_panel:child("input_text"):text())) > 0 then
			managers.chat:send_message(_ARG_0_._channel_id, 1, (_ARG_0_._input_panel:child("input_text"):text()))
		else
			_ARG_0_._enter_loose_focus_delay = true
			_ARG_0_:_loose_focus()
		end
		if not SimpleInput or SimpleInput._visible then
			_ARG_0_._input_panel:child("input_text"):set_text("")
			_ARG_0_._input_panel:child("input_text"):set_selection(0, 0)
		end
	end
	function SimpleInput._create_input_panel(_ARG_0_)
		_ARG_0_._input_panel = _ARG_0_._panel:panel({
			alpha = 0,
			name = "input_panel",
			x = 0,
			h = 24,
			w = _ARG_0_._panel_width,
			layer = 1
		})
		_ARG_0_._input_panel:rect({
			name = "focus_indicator",
			visible = false,
			color = Color.black:with_alpha(0.2),
			layer = 0
		})
		_ARG_0_._input_panel:text({
			name = "say",
			text = managers.SimpleInput.prompt,
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			x = 0,
			y = 0,
			align = "left",
			halign = "left",
			vertical = "center",
			hvertical = "center",
			blend_mode = "normal",
			color = Color.white,
			layer = 1
		}):set_size(_ARG_0_._input_panel:text({
			name = "say",
			text = managers.SimpleInput.prompt,
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			x = 0,
			y = 0,
			align = "left",
			halign = "left",
			vertical = "center",
			hvertical = "center",
			blend_mode = "normal",
			color = Color.white,
			layer = 1
		}):text_rect())
		_ARG_0_._input_panel:rect({
			name = "input_bg",
			color = Color.black:with_alpha(0.5),
			layer = -1,
			valign = "grow",
			h = _ARG_0_._input_panel:h()
		})
		_ARG_0_._input_panel:child("input_bg"):set_w(_ARG_0_._input_panel:w() - _ARG_0_._input_panel:text({
			name = "say",
			text = managers.SimpleInput.prompt,
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			x = 0,
			y = 0,
			align = "left",
			halign = "left",
			vertical = "center",
			hvertical = "center",
			blend_mode = "normal",
			color = Color.white,
			layer = 1
		}):text_rect())
		_ARG_0_._input_panel:child("input_bg"):set_x(_ARG_0_._input_panel:text({
			name = "say",
			text = managers.SimpleInput.prompt,
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			x = 0,
			y = 0,
			align = "left",
			halign = "left",
			vertical = "center",
			hvertical = "center",
			blend_mode = "normal",
			color = Color.white,
			layer = 1
		}):text_rect())
		_ARG_0_._input_panel:stop()
		_ARG_0_._input_panel:animate(callback(_ARG_0_, _ARG_0_, "_animate_hide_input"))
	end
	function SimpleInput._receive_message(_ARG_0_, _ARG_1_, _ARG_2_, ...)
		if not alive(_ARG_0_._panel) then
			return
		end
		if type(_ARG_1_) == "number" then
			if managers.SimpleInput.data then
				if managers.SimpleInput.cblkself then
				else
				end
			elseif managers.SimpleInput.cblkself then
			else
			end
		end
		if not pcall(managers.SimpleInput.cblk, _ARG_2_) or not pcall(managers.SimpleInput.cblk, _ARG_2_) then
			return
		end
		if pcall(managers.SimpleInput.cblk, _ARG_2_).icon then
		end
		_ARG_0_._panel:child("output_panel"):child("scroll_panel"):text({
			text = tostring(pcall(managers.SimpleInput.cblk, _ARG_2_).msg),
			font = pcall(managers.SimpleInput.cblk, _ARG_2_).font or tweak_data.menu.pd2_small_font,
			font_size = pcall(managers.SimpleInput.cblk, _ARG_2_).font_size or tweak_data.menu.pd2_small_font_size,
			x = _ARG_0_._panel:child("output_panel"):child("scroll_panel"):bitmap({
				texture = tweak_data.hud_icons:get_icon_data(pcall(managers.SimpleInput.cblk, _ARG_2_).icon)
			}):right(),
			y = 0,
			align = "left",
			halign = "left",
			vertical = "top",
			hvertical = "top",
			blend_mode = "normal",
			wrap = true,
			word_wrap = true,
			color = pcall(managers.SimpleInput.cblk, _ARG_2_).color or Color.white,
			layer = 0
		}):set_range_color(0, utf8.len(_ARG_0_._panel:child("output_panel"):child("scroll_panel"):text({
			text = tostring(pcall(managers.SimpleInput.cblk, _ARG_2_).msg),
			font = pcall(managers.SimpleInput.cblk, _ARG_2_).font or tweak_data.menu.pd2_small_font,
			font_size = pcall(managers.SimpleInput.cblk, _ARG_2_).font_size or tweak_data.menu.pd2_small_font_size,
			x = _ARG_0_._panel:child("output_panel"):child("scroll_panel"):bitmap({
				texture = tweak_data.hud_icons:get_icon_data(pcall(managers.SimpleInput.cblk, _ARG_2_).icon)
			}):right(),
			y = 0,
			align = "left",
			halign = "left",
			vertical = "top",
			hvertical = "top",
			blend_mode = "normal",
			wrap = true,
			word_wrap = true,
			color = pcall(managers.SimpleInput.cblk, _ARG_2_).color or Color.white,
			layer = 0
		}):text()), pcall(managers.SimpleInput.cblk, _ARG_2_).color or Color.white)
		_ARG_0_._panel:child("output_panel"):child("scroll_panel"):text({
			text = tostring(pcall(managers.SimpleInput.cblk, _ARG_2_).msg),
			font = pcall(managers.SimpleInput.cblk, _ARG_2_).font or tweak_data.menu.pd2_small_font,
			font_size = pcall(managers.SimpleInput.cblk, _ARG_2_).font_size or tweak_data.menu.pd2_small_font_size,
			x = _ARG_0_._panel:child("output_panel"):child("scroll_panel"):bitmap({
				texture = tweak_data.hud_icons:get_icon_data(pcall(managers.SimpleInput.cblk, _ARG_2_).icon)
			}):right(),
			y = 0,
			align = "left",
			halign = "left",
			vertical = "top",
			hvertical = "top",
			blend_mode = "normal",
			wrap = true,
			word_wrap = true,
			color = pcall(managers.SimpleInput.cblk, _ARG_2_).color or Color.white,
			layer = 0
		}):set_h(_ARG_0_._panel:child("output_panel"):child("scroll_panel"):text({
			text = tostring(pcall(managers.SimpleInput.cblk, _ARG_2_).msg),
			font = pcall(managers.SimpleInput.cblk, _ARG_2_).font or tweak_data.menu.pd2_small_font,
			font_size = pcall(managers.SimpleInput.cblk, _ARG_2_).font_size or tweak_data.menu.pd2_small_font_size,
			x = _ARG_0_._panel:child("output_panel"):child("scroll_panel"):bitmap({
				texture = tweak_data.hud_icons:get_icon_data(pcall(managers.SimpleInput.cblk, _ARG_2_).icon)
			}):right(),
			y = 0,
			align = "left",
			halign = "left",
			vertical = "top",
			hvertical = "top",
			blend_mode = "normal",
			wrap = true,
			word_wrap = true,
			color = pcall(managers.SimpleInput.cblk, _ARG_2_).color or Color.white,
			layer = 0
		}):text_rect())
		_ARG_0_._panel:child("output_panel"):child("scroll_panel"):rect({
			color = Color.black:with_alpha(0.5),
			layer = -1,
			halign = "left",
			hvertical = "top"
		}):set_h(_ARG_0_._panel:child("output_panel"):child("scroll_panel"):text({
			text = tostring(pcall(managers.SimpleInput.cblk, _ARG_2_).msg),
			font = pcall(managers.SimpleInput.cblk, _ARG_2_).font or tweak_data.menu.pd2_small_font,
			font_size = pcall(managers.SimpleInput.cblk, _ARG_2_).font_size or tweak_data.menu.pd2_small_font_size,
			x = _ARG_0_._panel:child("output_panel"):child("scroll_panel"):bitmap({
				texture = tweak_data.hud_icons:get_icon_data(pcall(managers.SimpleInput.cblk, _ARG_2_).icon)
			}):right(),
			y = 0,
			align = "left",
			halign = "left",
			vertical = "top",
			hvertical = "top",
			blend_mode = "normal",
			wrap = true,
			word_wrap = true,
			color = pcall(managers.SimpleInput.cblk, _ARG_2_).color or Color.white,
			layer = 0
		}):text_rect())
		table.insert(_ARG_0_._lines, {
			_ARG_0_._panel:child("output_panel"):child("scroll_panel"):text({
				text = tostring(pcall(managers.SimpleInput.cblk, _ARG_2_).msg),
				font = pcall(managers.SimpleInput.cblk, _ARG_2_).font or tweak_data.menu.pd2_small_font,
				font_size = pcall(managers.SimpleInput.cblk, _ARG_2_).font_size or tweak_data.menu.pd2_small_font_size,
				x = _ARG_0_._panel:child("output_panel"):child("scroll_panel"):bitmap({
					texture = tweak_data.hud_icons:get_icon_data(pcall(managers.SimpleInput.cblk, _ARG_2_).icon)
				}):right(),
				y = 0,
				align = "left",
				halign = "left",
				vertical = "top",
				hvertical = "top",
				blend_mode = "normal",
				wrap = true,
				word_wrap = true,
				color = pcall(managers.SimpleInput.cblk, _ARG_2_).color or Color.white,
				layer = 0
			}),
			_ARG_0_._panel:child("output_panel"):child("scroll_panel"):rect({
				color = Color.black:with_alpha(0.5),
				layer = -1,
				halign = "left",
				hvertical = "top"
			}),
			(_ARG_0_._panel:child("output_panel"):child("scroll_panel"):bitmap({
				texture = tweak_data.hud_icons:get_icon_data(pcall(managers.SimpleInput.cblk, _ARG_2_).icon)
			}))
		})
		_ARG_0_:_layout_output_panel()
		if not _ARG_0_._focus then
			_ARG_0_._panel:child("output_panel"):stop()
			_ARG_0_._panel:child("output_panel"):animate(callback(_ARG_0_, _ARG_0_, "_animate_show_component"), _ARG_0_._panel:child("output_panel"):alpha())
			_ARG_0_._panel:child("output_panel"):animate(callback(_ARG_0_, _ARG_0_, "_animate_fade_output"))
		end
	end
end