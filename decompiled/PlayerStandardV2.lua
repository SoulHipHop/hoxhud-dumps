function PlayerStandard._update_fwd_ray(_ARG_0_, ...)
	_UPVALUE0_._update_fwd_ray(_ARG_0_, ...)
	if game_state_machine:current_state_name() == "ingame_waiting_for_respawn" then
		managers.hud:set_enemy_health_visible(false)
		return
	end
	if not (_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit) or not managers.user:get_setting("hoxhud_show_civilian_health") and managers.enemy:is_civilian(_ARG_0_._fwd_ray.unit) then
		return
	end
	if _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:in_slot(8) and alive(_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent()) and _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent() or _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:character_damage() then
		if _ARG_0_._no_char_last_tick and type(_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:in_slot(8) and alive(_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent()) and _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent() or _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:character_damage()._health) == "number" and _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:in_slot(8) and alive(_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent()) and _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent() or _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:character_damage()._health <= 0 then
			return
		end
		_ARG_0_._last_char = _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:in_slot(8) and alive(_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent()) and _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent() or _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit
		managers.hud:set_enemy_health({
			current = (_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:in_slot(8) and alive(_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent()) and _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent() or _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:character_damage()._health or 0) * (tweak_data.hoxhud.show_multiplied_enemy_health and 10 or 1),
			total = (_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:in_slot(8) and alive(_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent()) and _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:parent() or _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:character_damage()._HEALTH_INIT or 0) * (tweak_data.hoxhud.show_multiplied_enemy_health and 10 or 1)
		})
		managers.hud:set_enemy_health_visible(true)
		_ARG_0_._no_char_last_tick = false
	else
		if alive(_ARG_0_._last_char) then
			managers.hud:set_enemy_health({
				current = (_ARG_0_._last_char:character_damage()._health or 0) * (tweak_data.hoxhud.show_multiplied_enemy_health and 10 or 1),
				total = (_ARG_0_._last_char:character_damage()._HEALTH_INIT or 0) * (tweak_data.hoxhud.show_multiplied_enemy_health and 10 or 1)
			})
		end
		managers.hud:set_enemy_health_visible(false)
		_ARG_0_._no_char_last_tick = true
	end
end
function PlayerStandard._start_action_throw_grenade(_ARG_0_, ...)
	tweak_data.hoxhud.allow_grenades_in_stealth = true
	return _UPVALUE0_._start_action_throw_grenade(_ARG_0_, ...)
end
function PlayerStandard._check_action_primary_attack(_ARG_0_, ...)
	tweak_data.hoxhud.allow_grenades_in_stealth = _ARG_0_._shooting and _ARG_0_._equipped_unit:base()._name_id == "gre_m79" and true or tweak_data.hoxhud.allow_grenades_in_stealth
	return unpack({
		_UPVALUE0_._check_action_primary_attack(_ARG_0_, ...)
	})
end
function PlayerStandard._start_action_reload(_ARG_0_, _ARG_1_, ...)
	_UPVALUE0_._start_action_reload(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_._equipped_unit:base():can_reload() and managers.user:get_setting("hoxhud_show_timer_on_reload") and not managers.user:get_setting("hoxhud_anticheat_only") and managers.player:current_state() ~= "bleed_out" then
		_ARG_0_._state_data._isReloading = true
		managers.hud:show_interaction_bar(0, _ARG_0_._state_data.reload_expire_t or 0)
		_ARG_0_._state_data.reload_offset = _ARG_1_
	end
end
function PlayerStandard._update_reload_timers(_ARG_0_, _ARG_1_, ...)
	_UPVALUE0_._update_reload_timers(_ARG_0_, _ARG_1_, ...)
	if not managers.user:get_setting("hoxhud_show_timer_on_reload") or managers.user:get_setting("hoxhud_anticheat_only") then
		return
	elseif not _ARG_0_._state_data.reload_expire_t and _ARG_0_._state_data._isReloading then
		managers.hud:hide_interaction_bar(true)
		_ARG_0_._state_data._isReloading = false
	elseif _ARG_0_._state_data._isReloading and managers.player:current_state() ~= "bleed_out" then
		managers.hud:set_interaction_bar_width(_ARG_1_ and _ARG_1_ - _ARG_0_._state_data.reload_offset or 0, _ARG_0_._state_data.reload_expire_t and _ARG_0_._state_data.reload_expire_t - _ARG_0_._state_data.reload_offset or 0)
	end
end
function PlayerStandard._interupt_action_reload(_ARG_0_, ...)
	if _ARG_0_._state_data._isReloading and managers.user:get_setting("hoxhud_show_timer_on_reload") and not managers.user:get_setting("hoxhud_anticheat_only") and managers.player:current_state() ~= "bleed_out" then
		managers.hud:hide_interaction_bar(false)
		_ARG_0_._state_data._isReloading = false
	end
	return _UPVALUE0_._interupt_action_reload(_ARG_0_, ...)
end
function PlayerStandard._update_melee_timers(_ARG_0_, _ARG_1_, ...)
	if not managers.user:get_setting("hoxhud_show_melee_charge_timer") then
		return _UPVALUE0_._update_melee_timers(_ARG_0_, _ARG_1_, ...)
	end
	if _ARG_0_._state_data.meleeing and not _ARG_0_._state_data.melee_attack_allowed_t and not tweak_data.blackmarket.melee_weapons[managers.blackmarket:equipped_melee_weapon()].instant then
		if math.clamp(_ARG_1_ - (_ARG_0_._state_data.melee_start_t or 0), 0, tweak_data.blackmarket.melee_weapons[managers.blackmarket:equipped_melee_weapon()].stats.charge_time) < 0.12 or _ARG_0_._state_data._at_max_melee then
		elseif math.clamp(_ARG_1_ - (_ARG_0_._state_data.melee_start_t or 0), 0, tweak_data.blackmarket.melee_weapons[managers.blackmarket:equipped_melee_weapon()].stats.charge_time) >= tweak_data.blackmarket.melee_weapons[managers.blackmarket:equipped_melee_weapon()].stats.charge_time then
			managers.hud:hide_interaction_bar(true)
			_ARG_0_._state_data._at_max_melee = true
		elseif math.clamp(_ARG_1_ - (_ARG_0_._state_data.melee_start_t or 0), 0, tweak_data.blackmarket.melee_weapons[managers.blackmarket:equipped_melee_weapon()].stats.charge_time) >= 0.12 and _ARG_0_._state_data._need_show_interact == nil then
			_ARG_0_._state_data._need_show_interact = true
		elseif _ARG_0_._state_data._need_show_interact then
			managers.hud:show_interaction_bar(0, tweak_data.blackmarket.melee_weapons[managers.blackmarket:equipped_melee_weapon()].stats.charge_time or 0)
			_ARG_0_._state_data._need_show_interact = false
		else
			managers.hud:set_interaction_bar_width(math.clamp(_ARG_1_ - (_ARG_0_._state_data.melee_start_t or 0), 0, tweak_data.blackmarket.melee_weapons[managers.blackmarket:equipped_melee_weapon()].stats.charge_time), tweak_data.blackmarket.melee_weapons[managers.blackmarket:equipped_melee_weapon()].stats.charge_time)
		end
	end
	return unpack({
		_UPVALUE0_._update_melee_timers(_ARG_0_, _ARG_1_, ...)
	})
end
function PlayerStandard.update(_ARG_0_, _ARG_1_, ...)
	managers.hud:update_inspire_expire(_ARG_0_._ext_movement:morale_boost() and managers.enemy:get_delayed_clbk_expire_t(_ARG_0_._ext_movement:morale_boost().expire_clbk_id) - _ARG_1_ or -1)
	return _UPVALUE0_.update(_ARG_0_, _ARG_1_, ...)
end
function PlayerStandard._do_melee_damage(_ARG_0_, _ARG_1_, ...)
	if managers.user:get_setting("hoxhud_show_melee_charge_timer") then
		managers.hud:hide_interaction_bar(false)
		_ARG_0_._state_data._need_show_interact = nil
		_ARG_0_._state_data._at_max_melee = nil
	end
	return _UPVALUE0_._do_melee_damage(_ARG_0_, _ARG_1_, ...)
end
function PlayerStandard._interupt_action_melee(_ARG_0_, ...)
	if managers.user:get_setting("hoxhud_show_melee_charge_timer") and _ARG_0_._state_data.meleeing then
		_ARG_0_._state_data._need_show_interact = nil
		_ARG_0_._state_data._at_max_melee = nil
		managers.hud:hide_interaction_bar(false)
	end
	_UPVALUE0_._interupt_action_melee(_ARG_0_, ...)
end
function PlayerStandard._stance_entered(_ARG_0_, ...)
	if managers.user:get_setting("hoxhud_rotate_for_angled_sight") then
		_ARG_0_._camera_unit:base():set_want_rotated(not _ARG_0_._state_data.in_steelsight and _ARG_0_._equipped_unit:base():is_second_sight_on() and not _ARG_0_:_is_reloading() and _ARG_0_._equipped_unit:base().weapon_tweak_data and _ARG_0_._equipped_unit:base():weapon_tweak_data().category == "snp")
		_ARG_0_._camera_unit:base():set_want_restored(not _ARG_0_._state_data.in_steelsight and (not _ARG_0_._equipped_unit:base():is_second_sight_on() or _ARG_0_:_is_reloading()))
		_ARG_0_._camera_unit:base():set_weapon_name(_ARG_0_._equipped_unit:base()._name_id)
	end
	return _UPVALUE0_._stance_entered(_ARG_0_, ...)
end
function PlayerStandard._get_input(_ARG_0_, ...)
	if managers.player:should_do_press_to_interact() then
		_UPVALUE0_._get_input(_ARG_0_, ...).btn_interact_release = _ARG_0_._controller:get_input_pressed("use_item")
		return (_UPVALUE0_._get_input(_ARG_0_, ...))
	else
		return _UPVALUE0_._get_input(_ARG_0_, ...)
	end
end
