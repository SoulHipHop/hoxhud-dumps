function CopDamage.init(_ARG_0_, ...)
	_ARG_0_._head_body_name = managers.user:get_setting("hoxhud_headshot_gore") and "head" or _ARG_0_._head_body_name
	_UPVALUE0_.init(_ARG_0_, ...)
	_ARG_0_._hud = HUDCopDamage:new(_ARG_0_._unit)
end
function CopDamage._want_decapitation(_ARG_0_, _ARG_1_)
	if managers.user:get_setting("hoxhud_headshot_gore") ~= "high_dmg" or not _ARG_1_ then
		return managers.user:get_setting("hoxhud_headshot_gore") == "all" or not _ARG_1_
	end
	return tweak_data.hoxhud.decapitation_categories[_ARG_1_:base():weapon_tweak_data().category] or tweak_data.hoxhud.decapitation_weapons[_ARG_1_:base().name_id]
end
function CopDamage.damage_bullet(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_._dead or _ARG_0_._invulnerable then
		return
	end
	if _ARG_1_.attacker_unit == managers.player:player_unit() then
		managers.statistics:on_enemy_damaged(_ARG_0_._unit, _ARG_1_.attacker_unit, _ARG_1_.damage, true)
		if _ARG_1_.damage > 0.09 then
			_ARG_0_._hud:show_damage(#{
				_UPVALUE0_.damage_bullet(_ARG_0_, _ARG_1_, ...)
			} > 0 and _ARG_1_.damage or 0, _ARG_0_._dead, _ARG_0_._head_body_name and _ARG_1_.col_ray.body and _ARG_1_.col_ray.body:name() == _ARG_0_._ids_head_body_name)
		end
	end
	if {
		_UPVALUE0_.damage_bullet(_ARG_0_, _ARG_1_, ...)
	}[1] and {
		_UPVALUE0_.damage_bullet(_ARG_0_, _ARG_1_, ...)
	}[1].type and {
		_UPVALUE0_.damage_bullet(_ARG_0_, _ARG_1_, ...)
	}[1].type == "death" and _ARG_0_._head_body_name and _ARG_1_.col_ray.body and _ARG_1_.col_ray.body:name() == _ARG_0_._ids_head_body_name then
		if _ARG_1_.attacker_unit == managers.player:player_unit() then
			managers.hud:on_headshot_confirmed()
		end
		if _ARG_0_:_want_decapitation(_ARG_1_.weapon_unit) then
			_ARG_0_:process_head_kill()
		end
	end
	return unpack({
		_UPVALUE0_.damage_bullet(_ARG_0_, _ARG_1_, ...)
	})
end
function CopDamage.damage_melee(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_._dead or _ARG_0_._invulnerable then
		return
	end
	_ARG_1_.damage = Global.god_mode and 10000 or _ARG_1_.damage
	if _ARG_1_.attacker_unit == managers.player:player_unit() then
		managers.statistics:on_enemy_damaged(_ARG_0_._unit, _ARG_1_.attacker_unit, _ARG_1_.damage, true)
		if _ARG_1_.damage > 0.09 then
			_ARG_0_._hud:show_damage(_ARG_1_.damage, _ARG_0_._dead, _ARG_0_._head_body_name and _ARG_1_.col_ray.body and _ARG_1_.col_ray.body:name() == _ARG_0_._ids_head_body_name)
		end
	end
	if {
		_UPVALUE0_.damage_melee(_ARG_0_, _ARG_1_, ...)
	}[1] and {
		_UPVALUE0_.damage_melee(_ARG_0_, _ARG_1_, ...)
	}[1].type and {
		_UPVALUE0_.damage_melee(_ARG_0_, _ARG_1_, ...)
	}[1].type == "death" and _ARG_1_.name_id == "barbedwire" and _ARG_0_._head_body_name and _ARG_1_.col_ray.body and _ARG_1_.col_ray.body:name() == _ARG_0_._ids_head_body_name then
		if _ARG_0_:_want_decapitation(_ARG_1_.weapon_unit) then
			_ARG_0_:process_head_kill()
		end
		if managers.user:get_setting("hoxhud_batter_up") and (not managers.groupai:state():whisper_mode() or managers.enemy:get_number_of_enemies() == 0) then
			_ARG_0_:batter_up(_ARG_1_)
		end
	end
	return unpack({
		_UPVALUE0_.damage_melee(_ARG_0_, _ARG_1_, ...)
	})
end
function CopDamage.damage_explosion(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_._dead or _ARG_0_._invulnerable then
		return
	end
	if _ARG_1_.attacker_unit and _ARG_1_.attacker_unit == managers.player:player_unit() then
		managers.statistics:on_enemy_damaged(_ARG_0_._unit, _ARG_1_.attacker_unit, _ARG_1_.damage, true)
		if _ARG_1_.damage > 0.09 then
			_ARG_0_._hud:show_damage(_ARG_1_.damage, _ARG_0_._dead, false)
		end
	end
	return unpack({
		_UPVALUE0_.damage_explosion(_ARG_0_, _ARG_1_, ...)
	})
end
function CopDamage.do_decapitate_push(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_)
	if _ARG_1_:movement()._active_actions[1] and _ARG_1_:movement()._active_actions[1]:type() == "hurt" then
		_ARG_1_:movement()._active_actions[1]:force_ragdoll()
	end
	for _FORV_15_ = 0, _ARG_1_:num_bodies() - 1 do
		if _ARG_1_:body(_FORV_15_):enabled() and _ARG_1_:body(_FORV_15_):dynamic() then
			World:play_physic_effect(Idstring("physic_effects/body_explosion"), _ARG_1_:body(_FORV_15_), _ARG_3_:with_z(_ARG_3_.z + 0.5) * 600 * math.clamp(1 - _ARG_4_ / 500, 0.5, 1), 4 * _ARG_1_:body(_FORV_15_):mass() / math.random(2), (_ARG_3_:cross(math.UP) + math.UP * (0.5 * (math.random(2) == 1 and 1 or -1))) * (-1000 * math.sign(mvector3.distance(_ARG_2_, _ARG_1_:position()) - 100)), 1 + math.rand(2))
		end
	end
end
function CopDamage.batter_up(_ARG_0_, _ARG_1_)
	mvector3.set_z(mvector3.copy(_ARG_0_._unit:movement():m_pos()), mvector3.copy(_ARG_0_._unit:movement():m_pos()).z + math.random(150, 220))
	_ARG_0_:do_decapitate_push(_ARG_0_._unit, mvector3.copy(_ARG_0_._unit:movement():m_pos()), mvector3.copy(_ARG_0_._unit:movement():m_pos()) - _ARG_1_.attacker_unit:movement():m_head_pos(), mvector3.normalize(mvector3.copy(_ARG_0_._unit:movement():m_pos()) - _ARG_1_.attacker_unit:movement():m_head_pos()))
end
function CopDamage.sync_damage_melee(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, ...)
	if managers.user:get_setting("hoxhud_batter_up") and _ARG_4_ == 2 and (not managers.groupai:state():whisper_mode() or managers.enemy:get_number_of_enemies() == 0) then
		_ARG_0_:batter_up({attacker_unit = _ARG_1_})
	end
	return unpack(_UPVALUE0_.sync_damage_melee(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, 1, _ARG_5_, _ARG_6_, _ARG_7_, ...) or {})
end
function CopDamage._send_melee_attack_result(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, ...)
	_ARG_0_._unit:network():send("damage_melee", _ARG_1_.attacker_unit, _ARG_2_, _ARG_3_, _ARG_1_.name_id == "barbedwire" and _ARG_0_._dead and _ARG_0_._head_body_name and _ARG_1_.col_ray.body and _ARG_1_.col_ray.body:name() == _ARG_0_._ids_head_body_name and (Network:is_client() or not managers.groupai:state():whisper_mode() or managers.enemy:get_number_of_enemies() == 0) and 2 or 1, _ARG_4_, _ARG_5_, _ARG_0_._dead and true or false)
end
