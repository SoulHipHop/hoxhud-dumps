function GrenadeBase.add_damage_result(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	if _ARG_2_ and _ARG_0_._thrower_unit and _ARG_0_._thrower_unit ~= managers.player:player_unit() then
		pcall(UnitNetworkHandler._record_ai_death, UnitNetworkHandler, _ARG_1_, _ARG_0_._thrower_unit)
	end
	managers.statistics:on_enemy_damaged(_ARG_1_, _ARG_0_._thrower_unit, _ARG_3_)
	return _UPVALUE0_.add_damage_result(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
end
