function MoneyManager.get_unsecured_bonus_bag_value(_ARG_0_, _ARG_1_, _ARG_2_)
	managers.loot:set_loot_check_override(true)
	managers.loot:set_loot_check_override(nil)
	return (_ARG_0_:get_secured_bonus_bag_value(_ARG_1_, _ARG_2_))
end
