HUDEnemyTarget = class()
function HUDEnemyTarget.init(_ARG_0_, _ARG_1_)
	_ARG_0_._hud_panel = _ARG_1_.panel
	_ARG_0_._no_target = true
	if _ARG_0_._hud_panel:child("enemy_target_panel") then
		_ARG_0_._hud_panel:child("enemy_target_panel"):remove()
	end
	_ARG_0_._enemy_target_panel = _ARG_0_._hud_panel:panel({
		visible = true,
		name = "enemy_target_panel",
		h = managers.user:get_setting("hoxhud_enemy_health_size") + 20,
		y = managers.user:get_setting("hoxhud_show_enemy_health") and managers.user:get_setting("hoxhud_enemy_health_vert") or -1000,
		valign = "top",
		layer = 0
	})
	_ARG_0_._enemy_target_panel:set_x(_ARG_0_._enemy_target_panel:x() + managers.user:get_setting("hoxhud_enemy_health_horz"))
	_ARG_0_._enemy_target_panel:panel({
		name = "radial_health_panel",
		visible = false,
		layer = 1,
		w = managers.user:get_setting("hoxhud_enemy_health_size"),
		h = managers.user:get_setting("hoxhud_enemy_health_size"),
		x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
	}):set_bottom(_ARG_0_._enemy_target_panel:h())
	_ARG_0_._radial_health_panel = _ARG_0_._enemy_target_panel:panel({
		name = "radial_health_panel",
		visible = false,
		layer = 1,
		w = managers.user:get_setting("hoxhud_enemy_health_size"),
		h = managers.user:get_setting("hoxhud_enemy_health_size"),
		x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
	})
	_ARG_0_._radial_health = _ARG_0_._enemy_target_panel:panel({
		name = "radial_health_panel",
		visible = false,
		layer = 1,
		w = managers.user:get_setting("hoxhud_enemy_health_size"),
		h = managers.user:get_setting("hoxhud_enemy_health_size"),
		x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
	}):bitmap({
		name = "radial_health",
		texture = "guis/textures/pd2/hud_health",
		texture_rect = {
			64,
			0,
			-64,
			64
		},
		render_template = "VertexColorTexturedRadial",
		align = "center",
		blend_mode = "normal",
		alpha = 1,
		w = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):w(),
		h = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):h(),
		layer = 2
	})
	_ARG_0_._radial_health:set_color(Color(1, 1, 1, 1))
	_ARG_0_._damage_indicator = _ARG_0_._enemy_target_panel:panel({
		name = "radial_health_panel",
		visible = false,
		layer = 1,
		w = managers.user:get_setting("hoxhud_enemy_health_size"),
		h = managers.user:get_setting("hoxhud_enemy_health_size"),
		x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
	}):bitmap({
		name = "damage_indicator",
		texture = "guis/textures/pd2/hud_radial_rim",
		blend_mode = "add",
		alpha = 0,
		w = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):w(),
		h = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):h(),
		layer = 1,
		align = "center"
	})
	_ARG_0_._damage_indicator:set_color(Color(1, 1, 1, 1))
	_ARG_0_._damage_bonus_glow = _ARG_0_._enemy_target_panel:panel({
		name = "radial_health_panel",
		visible = false,
		layer = 1,
		w = managers.user:get_setting("hoxhud_enemy_health_size"),
		h = managers.user:get_setting("hoxhud_enemy_health_size"),
		x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
	}):panel({
		visible = true,
		name = "damage_glow_panel",
		h = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):h() - 2,
		w = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):w() - 2,
		layer = 0
	}):bitmap({
		name = "glow",
		texture = "guis/textures/pd2/crimenet_marker_glow",
		texture_rect = {
			1,
			1,
			62,
			62
		},
		h = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):h() - 0,
		w = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):w() - 0,
		color = Color.red,
		blend_mode = "add",
		layer = 2,
		align = "center",
		visible = false,
		rotation = 360
	})
	_ARG_0_._damage_bonus_glow:set_position(0 / 2, 0 / 2)
	_ARG_0_._health_num = _ARG_0_._enemy_target_panel:panel({
		name = "radial_health_panel",
		visible = false,
		layer = 1,
		w = managers.user:get_setting("hoxhud_enemy_health_size"),
		h = managers.user:get_setting("hoxhud_enemy_health_size"),
		x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
	}):text({
		name = "health_num",
		text = "",
		layer = 5,
		alpha = 0.9,
		color = Color.white,
		w = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):w(),
		x = 0,
		y = 0,
		h = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):h(),
		vertical = "center",
		align = "center",
		font_size = 30,
		font = tweak_data.menu.pd2_large_font
	})
	_ARG_0_._health_num_bg1 = _ARG_0_._enemy_target_panel:panel({
		name = "radial_health_panel",
		visible = false,
		layer = 1,
		w = managers.user:get_setting("hoxhud_enemy_health_size"),
		h = managers.user:get_setting("hoxhud_enemy_health_size"),
		x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
	}):text({
		name = "health_num_bg1",
		text = "",
		layer = 4,
		alpha = 0.9,
		color = Color.black,
		w = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):w(),
		x = 0,
		y = 0,
		h = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):h(),
		vertical = "center",
		align = "center",
		font_size = 30,
		font = tweak_data.menu.pd2_large_font
	})
	_ARG_0_._health_num_bg2 = _ARG_0_._enemy_target_panel:panel({
		name = "radial_health_panel",
		visible = false,
		layer = 1,
		w = managers.user:get_setting("hoxhud_enemy_health_size"),
		h = managers.user:get_setting("hoxhud_enemy_health_size"),
		x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
	}):text({
		name = "health_num_bg2",
		text = "",
		layer = 4,
		alpha = 0.9,
		color = Color.black,
		w = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):w(),
		x = 0,
		y = 0,
		h = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):h(),
		vertical = "center",
		align = "center",
		font_size = 30,
		font = tweak_data.menu.pd2_large_font
	})
	_ARG_0_._health_num_bg3 = _ARG_0_._enemy_target_panel:panel({
		name = "radial_health_panel",
		visible = false,
		layer = 1,
		w = managers.user:get_setting("hoxhud_enemy_health_size"),
		h = managers.user:get_setting("hoxhud_enemy_health_size"),
		x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
	}):text({
		name = "health_num_bg3",
		text = "",
		layer = 4,
		alpha = 0.9,
		color = Color.black,
		w = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):w(),
		x = 0,
		y = 0,
		h = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):h(),
		vertical = "center",
		align = "center",
		font_size = 30,
		font = tweak_data.menu.pd2_large_font
	})
	_ARG_0_._health_num_bg4 = _ARG_0_._enemy_target_panel:panel({
		name = "radial_health_panel",
		visible = false,
		layer = 1,
		w = managers.user:get_setting("hoxhud_enemy_health_size"),
		h = managers.user:get_setting("hoxhud_enemy_health_size"),
		x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
	}):text({
		name = "health_num_bg4",
		text = "",
		layer = 4,
		alpha = 0.9,
		color = Color.black,
		w = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):w(),
		x = 0,
		y = 0,
		h = _ARG_0_._enemy_target_panel:panel({
			name = "radial_health_panel",
			visible = false,
			layer = 1,
			w = managers.user:get_setting("hoxhud_enemy_health_size"),
			h = managers.user:get_setting("hoxhud_enemy_health_size"),
			x = _ARG_0_._enemy_target_panel:center() - managers.user:get_setting("hoxhud_enemy_health_size") / 2
		}):h(),
		vertical = "center",
		align = "center",
		font_size = 30,
		font = tweak_data.menu.pd2_large_font
	})
	_ARG_0_._health_num_bg1:set_y(_ARG_0_._health_num_bg1:y() - 1)
	_ARG_0_._health_num_bg1:set_x(_ARG_0_._health_num_bg1:x() - 1)
	_ARG_0_._health_num_bg2:set_y(_ARG_0_._health_num_bg2:y() + 1)
	_ARG_0_._health_num_bg2:set_x(_ARG_0_._health_num_bg2:x() + 1)
	_ARG_0_._health_num_bg3:set_y(_ARG_0_._health_num_bg3:y() + 1)
	_ARG_0_._health_num_bg3:set_x(_ARG_0_._health_num_bg3:x() - 1)
	_ARG_0_._health_num_bg4:set_y(_ARG_0_._health_num_bg4:y() - 1)
	_ARG_0_._health_num_bg4:set_x(_ARG_0_._health_num_bg4:x() + 1)
end
function HUDEnemyTarget.set_health(_ARG_0_, _ARG_1_)
	if not _ARG_1_ or not _ARG_1_.current or not _ARG_1_.total then
		return
	end
	if _ARG_1_.current / _ARG_1_.total < _ARG_0_._radial_health:color().red then
		_ARG_0_._damage_was_fatal = _ARG_1_.current / _ARG_1_.total <= 0 and true or false
		_ARG_0_:_damage_taken()
	end
	_ARG_0_._radial_health:set_color(Color(1, _ARG_1_.current / _ARG_1_.total, 1, 1))
	if not (_ARG_1_.current <= 0) or not "" then
	end
	_ARG_0_._health_num:set_text((string.format(managers.user:get_setting("hoxhud_show_enemy_health_mul") and "%.0f" or "%.1f", managers.user:get_setting("hoxhud_show_enemy_health_mul") and _ARG_1_.current * 10 or _ARG_1_.current)))
	for _FORV_8_ = 1, 4 do
		_ARG_0_["_health_num_bg" .. _FORV_8_]:set_text((string.format(managers.user:get_setting("hoxhud_show_enemy_health_mul") and "%.0f" or "%.1f", managers.user:get_setting("hoxhud_show_enemy_health_mul") and _ARG_1_.current * 10 or _ARG_1_.current)))
	end
end
function HUDEnemyTarget.set_visible(_ARG_0_, _ARG_1_)
	if _ARG_0_._no_target then
		if _ARG_1_ then
			_ARG_0_._no_target = false
			_ARG_0_._radial_health_panel:set_visible(true)
		end
	elseif not _ARG_1_ then
		_ARG_0_._no_target = true
		_ARG_0_._radial_health_panel:animate(callback(_ARG_0_, _ARG_0_, "_animate_hide_decay"))
	end
end
function HUDEnemyTarget.set_has_damage_bonus(_ARG_0_, _ARG_1_, _ARG_2_)
	if _ARG_2_ ~= _ARG_0_._strong_bonus then
		if not _ARG_2_ or not Color[managers.user:get_setting("hoxhud_overkill_dmg_boost_col")] then
		end
		_ARG_0_._damage_bonus_glow:set_color(Color[managers.user:get_setting("hoxhud_undrdog_dmg_boost_col")])
		_ARG_0_._strong_bonus = _ARG_2_
	end
	if _ARG_1_ == _ARG_0_._has_damage_bonus then
		return
	end
	_ARG_0_._has_damage_bonus = _ARG_1_
	_ARG_0_._damage_bonus_glow:set_visible(_ARG_1_)
	_ARG_0_._damage_bonus_glow:stop()
	if _ARG_1_ then
		_ARG_0_._damage_bonus_glow:animate(callback(_ARG_0_, HUDTeammate, "_animate_glow"), {
			_ARG_0_._damage_bonus_glow
		}, "_has_damage_bonus", tweak_data.hoxhud.damage_boost_flash_speed)
	end
end
function HUDEnemyTarget._animate_hide_decay(_ARG_0_, _ARG_1_)
	while 1.5 > 0 and _ARG_0_._no_target do
		_ARG_1_:set_alpha((1.5 - coroutine.yield()) / 1.5)
	end
	if _ARG_0_._no_target then
		_ARG_1_:set_visible(false)
	end
	_ARG_1_:set_alpha(1)
end
function HUDEnemyTarget._damage_taken(_ARG_0_)
	_ARG_0_._damage_indicator:stop()
	_ARG_0_._damage_indicator:animate(callback(_ARG_0_, _ARG_0_, "_animate_damage_taken"))
end
function HUDEnemyTarget._animate_damage_taken(_ARG_0_, _ARG_1_)
	_ARG_0_:__animate_damage_taken(_ARG_0_._damage_was_fatal, _ARG_1_)
end
function HUDEnemyTarget.__animate_damage_taken(_ARG_0_, _ARG_1_, _ARG_2_)
	_ARG_2_:set_alpha(1)
	while 1.5 > 0 do
		if not _ARG_1_ or not Color(math.clamp(0.5 - coroutine.yield(), 0, 1) / 0.5 + Color[managers.user:get_setting("hoxhud_enemy_kill_color")].r, math.clamp(0.5 - coroutine.yield(), 0, 1) / 0.5 + Color[managers.user:get_setting("hoxhud_enemy_kill_color")].g, math.clamp(0.5 - coroutine.yield(), 0, 1) / 0.5 + Color[managers.user:get_setting("hoxhud_enemy_kill_color")].b) then
		end
		_ARG_2_:set_color((Color(math.clamp(0.5 - coroutine.yield(), 0, 1) / 0.5 + Color[managers.user:get_setting("hoxhud_enemy_hurt_color")].r, math.clamp(0.5 - coroutine.yield(), 0, 1) / 0.5 + Color[managers.user:get_setting("hoxhud_enemy_hurt_color")].g, math.clamp(0.5 - coroutine.yield(), 0, 1) / 0.5 + Color[managers.user:get_setting("hoxhud_enemy_hurt_color")].b)))
		_ARG_2_:set_alpha((1.5 - coroutine.yield()) / 1.5)
	end
	_ARG_2_:set_alpha(0)
end
