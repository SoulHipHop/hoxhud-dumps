LoadoutMusic = "HoxHud\\loadout_music_clipped.mp3"
function IngameWaitingForPlayersState.repeat_loadout_music()
	IngameWaitingForPlayersState._custom_music:stop()
	IngameWaitingForPlayersState._custom_music:queuefile(LoadoutMusic, 84900)
	IngameWaitingForPlayersState._custom_music:setcallback(IngameWaitingForPlayersState.repeat_loadout_music)
	IngameWaitingForPlayersState._custom_music:play()
	IngameWaitingForPlayersState._custom_music:set_volume(managers.user:get_setting("music_volume") or Global.music_manager.volume and Global.music_manager.volume * 100 or 1)
end
function IngameWaitingForPlayersState.at_enter(_ARG_0_, ...)
	_UPVALUE0_.at_enter(_ARG_0_, ...)
	if not io.open(LoadoutMusic, "r") or not PlayMediaV2 then
		return
	end
	io.open(LoadoutMusic, "r"):close()
	managers.music:stop()
	IngameWaitingForPlayersState._custom_music = PlayMediaV2(LoadoutMusic)
	_ARG_0_._custom_music:setcallback(_ARG_0_.repeat_loadout_music)
	_ARG_0_._custom_music:play()
	_ARG_0_._custom_music:set_volume(managers.user:get_setting("music_volume") or Global.music_manager.volume and Global.music_manager.volume * 100 or 1)
end
function IngameWaitingForPlayersState.sync_start(_ARG_0_, ...)
	_UPVALUE0_.sync_start(_ARG_0_, ...)
	if _ARG_0_._custom_music then
		_ARG_0_._custom_music:stop()
		_ARG_0_._custom_music = nil
	end
end
function IngameWaitingForPlayersState.at_exit(_ARG_0_, ...)
	_UPVALUE0_.at_exit(_ARG_0_, ...)
	if _ARG_0_._custom_music then
		_ARG_0_._custom_music:stop()
		_ARG_0_._custom_music = nil
	end
end
function IngameWaitingForPlayersState._start_audio(_ARG_0_, ...)
	if not io.open("HoxHud/hoxhud-initialised.mp3", "r") then
		return _UPVALUE0_._start_audio(_ARG_0_, ...)
	end
	io.open("HoxHud/hoxhud-initialised.mp3", "r"):close()
	if (io.open("HoxHud/.crashcheck", "r") and true or false) and io.open("HoxHud/.crashcheck", "r") or io.open("HoxHud/.crashcheck", "w"):read("*all") ~= "OK" then
		io.open("HoxHud/.crashcheck", "r") or io.open("HoxHud/.crashcheck", "w"):close()
		return _UPVALUE0_._start_audio(_ARG_0_, ...)
	elseif io.open("HoxHud/.crashcheck", "r") and true or false then
		io.open("HoxHud/.crashcheck", "r") or io.open("HoxHud/.crashcheck", "w"):close()
	end
	function managers.briefing.post_event(...)
		_UPVALUE0_._hoxhudInit = PlayMediaV2("HoxHud/hoxhud-initialised.mp3")
		_UPVALUE0_._hoxhudInit:set_volume(managers.user:get_setting("sfx_volume") or Global.music_manager.volume and Global.music_manager.volume * 100 or 1)
		_UPVALUE0_._hoxhudInit:setcallback(function()
			if _UPVALUE0_ then
				_UPVALUE0_:write("OK")
				_UPVALUE0_:close()
			end
			_UPVALUE1_._hoxhudInit = nil
			if game_state_machine:current_state_name() == "ingame_waiting_for_players" then
				_UPVALUE2_(unpack(_UPVALUE3_))
			end
		end)
		_UPVALUE0_._hoxhudInit:play()
		return true
	end
	managers.briefing.post_event = managers.briefing.post_event
	return unpack({
		_UPVALUE0_._start_audio(_ARG_0_, ...)
	})
end
function IngameWaitingForPlayersState._start_delay(_ARG_0_)
	_ARG_0_._delay_start_t = _ARG_0_._delay_start_t or Application:time() + 2
end
