HUDTimerManager = class()
HUDTimerElement = class()
function HUDTimerElement.init(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_)
	if not managers.user:get_setting("hoxhud_show_timer_panel") then
		return
	end
	if not _ARG_7_ or _ARG_7_ == "" or not _ARG_7_ then
		_ARG_7_ = "unknown"
	end
	_ARG_0_._tweak_name = _ARG_8_.tweak_name
	_ARG_0_._panel = _ARG_1_
	_ARG_0_._text_color = _ARG_8_.text_color or Color.white
	_ARG_0_._timer_color = _ARG_8_.timer_color or Color.white
	_ARG_0_._timer_flash = _ARG_8_.timer_flash or Color.red
	_ARG_0_._text_flash = _ARG_8_.text_flash or _ARG_0_._timer_flash
	_ARG_0_._timer_complete = _ARG_8_.timer_complete or Color.green
	_ARG_0_._flash_period = _ARG_8_.flash_period or 0.5
	_ARG_0_._timer_panel = _ARG_1_:panel({
		name = "hud_timer_element_" .. _ARG_2_,
		h = _ARG_4_,
		w = _ARG_5_ <= _ARG_1_:w() and _ARG_5_ or _ARG_1_:w(),
		y = _ARG_6_,
		layer = 1
	})
	_ARG_0_._timer_circle = CircleBitmapGuiObject:new(_ARG_0_._timer_panel, {
		use_bg = true,
		align = "center",
		rotation = 360,
		radius = _ARG_3_ / 2,
		blend_mode = "normal",
		color = Color.white,
		layer = 1,
		x = _ARG_5_ / 2 - _ARG_3_ / 2,
		y = 0
	})
	_ARG_0_._timer_text = _ARG_0_._timer_panel:text({
		name = "timer_text",
		layer = 1,
		color = _ARG_0_._timer_color,
		align = "center",
		vertical = "center",
		valign = "center",
		visible = true,
		font_size = tweak_data.hud_players.name_size,
		font = tweak_data.menu.pd2_large_font
	})
	_ARG_0_._timer_text:set_center_y(_ARG_3_ / 2)
	_ARG_0_._timer_name = _ARG_0_._timer_panel:text({
		name = "timer_name",
		layer = 1,
		word_wrap = true,
		color = _ARG_0_._text_color,
		align = "center",
		y = _ARG_3_,
		visible = true,
		font_size = tweak_data.hud_players.name_size,
		font = tweak_data.hud_players.name_font,
		text = "THERMAL DRILL"
	})
	_ARG_0_._timer_name:set_text(utf8.to_upper(_ARG_7_))
	_ARG_0_._timer_format = "%.1fs"
	_ARG_0_._migrate_on_jammed = not _ARG_8_.dont_migrate_on_jammed
	_ARG_0_:apply_text_scaling()
end
function HUDTimerElement.apply_text_scaling(_ARG_0_)
	if _ARG_0_._timer_name:text_rect() > _ARG_0_._timer_panel:w() and Idstring((_ARG_0_._timer_name:text())) ~= Idstring(_ARG_0_._timer_name:text():gsub(" ", "\n")) then
		_ARG_0_._timer_name:set_text(_ARG_0_._timer_name:text():gsub(" ", "\n", 1))
		if _ARG_0_._timer_panel:w() < _ARG_0_._timer_name:text_rect() then
			_ARG_0_._timer_name:set_text(_ARG_0_._timer_name:text():reverse():gsub(" ", "\n", 1):reverse())
			if _ARG_0_._timer_name:text_rect() < _ARG_0_._timer_name:text_rect() then
				_ARG_0_._timer_name:set_text(_ARG_0_._timer_name:text():gsub(" ", "\n", 1))
			end
		end
	end
	while _ARG_0_._timer_name:text_rect() > _ARG_0_._timer_panel:h() - _ARG_0_._timer_name:y() or _ARG_0_._timer_panel:w() < _ARG_0_._timer_name:text_rect() do
		_ARG_0_._timer_name:set_font_size(_ARG_0_._timer_name:font_size() - 0.5)
		if _ARG_0_._timer_name:font_size() < 13 and not nil then
			_ARG_0_._timer_name:set_text(_ARG_0_._timer_name:text():gsub(" ", "\n"))
			_ARG_0_._timer_name:set_font_size(tweak_data.hud_players.name_size)
		end
	end
end
function HUDTimerElement.set_visible(_ARG_0_, _ARG_1_)
	if _ARG_0_._timer_panel then
		if not managers.user:get_setting("hoxhud_show_timer_panel") then
		end
		_ARG_0_._timer_panel:set_visible(_ARG_1_)
		if _ARG_0_._manager then
			_ARG_0_._manager:_layout_panels()
		end
	end
end
function HUDTimerElement.visible(_ARG_0_)
	return _ARG_0_._timer_panel:visible()
end
function HUDTimerElement.set_timer_format(_ARG_0_, _ARG_1_)
	_ARG_0_._timer_format = _ARG_1_
end
function HUDTimerElement.set_timer_flash(_ARG_0_, _ARG_1_)
	_ARG_0_._timer_flash = _ARG_1_
end
function HUDTimerElement.set_text_flash(_ARG_0_, _ARG_1_)
	_ARG_0_._text_flash = _ARG_1_
end
function HUDTimerElement.set_text_color(_ARG_0_, _ARG_1_)
	if _ARG_0_._timer_name then
		_ARG_0_._text_color = _ARG_1_
		_ARG_0_._timer_name:set_color(_ARG_1_)
	end
end
function HUDTimerElement.set_name_text(_ARG_0_, _ARG_1_)
	if _ARG_0_._timer_name then
		_ARG_0_._timer_name:set_text(utf8.to_upper(_ARG_1_))
		_ARG_0_:apply_text_scaling()
	end
end
function HUDTimerElement.set_timer_color(_ARG_0_, _ARG_1_)
	if _ARG_0_._timer_text then
		_ARG_0_._timer_color = _ARG_1_
		_ARG_0_._timer_text:set_color(_ARG_1_)
	end
end
function HUDTimerElement.set_completion(_ARG_0_, _ARG_1_, _ARG_2_)
	_ARG_1_ = _ARG_1_ or 0
	_ARG_2_ = _ARG_2_ or 0
	_ARG_0_._current = _ARG_1_
	_ARG_0_._total = _ARG_2_
	if _ARG_0_._timer_panel then
		if not _ARG_0_._migrated then
			if _ARG_1_ < (tweak_data.hoxhud.tab_screen_timers[_ARG_0_._tweak_name] or managers.user:get_setting("hoxhud_tabscreen_migrate")) then
				_ARG_0_._manager:migrate_timer(_ARG_0_)
				_ARG_0_._migrated = true
			end
		end
		_ARG_0_._timer_circle:set_current(_ARG_1_ / _ARG_2_)
		_ARG_0_._timer_text:set_text(string.format(_ARG_0_._timer_format, _ARG_1_ >= 0 and _ARG_1_ or 0))
		if not _ARG_0_._jammed then
			_ARG_0_._timer_text:set_color(Color(_ARG_1_ / _ARG_2_ + _ARG_0_._timer_complete.a, _ARG_1_ / _ARG_2_ + _ARG_0_._timer_complete.r, _ARG_1_ / _ARG_2_ + _ARG_0_._timer_complete.g, _ARG_1_ / _ARG_2_ + _ARG_0_._timer_complete.b))
		end
	end
end
function HUDTimerElement.set_jammed(_ARG_0_, _ARG_1_)
	if _ARG_0_._timer_panel then
		if _ARG_0_._migrate_on_jammed and not _ARG_0_._migrated then
			_ARG_0_._manager:migrate_timer(_ARG_0_)
			_ARG_0_:set_completion(_ARG_0_._current, _ARG_0_._total)
		end
		_ARG_0_._jammed = _ARG_1_
		_ARG_0_._timer_panel:stop()
		_ARG_0_._timer_name:set_color(_ARG_0_._text_color)
		if _ARG_1_ then
			_ARG_0_._timer_panel:animate(callback(_ARG_0_, _ARG_0_, "_animate_jammed_counter"))
		end
	end
end
function HUDTimerElement.is_jammed(_ARG_0_)
	return _ARG_0_._jammed
end
function HUDTimerElement.set_migrated(_ARG_0_, _ARG_1_)
	_ARG_0_._migrated = _ARG_1_
end
function HUDTimerElement._animate_jammed_counter(_ARG_0_)
	while _ARG_0_._jammed do
		if not _ARG_0_._timer_panel then
			return
		end
		_ARG_0_._timer_name:set_color(Color(math.abs(math.sin((_ARG_0_._flash_period + coroutine.yield()) * 360 * _ARG_0_._flash_period / 4)) + _ARG_0_._timer_flash.a, math.abs(math.sin((_ARG_0_._flash_period + coroutine.yield()) * 360 * _ARG_0_._flash_period / 4)) + _ARG_0_._timer_flash.r, math.abs(math.sin((_ARG_0_._flash_period + coroutine.yield()) * 360 * _ARG_0_._flash_period / 4)) + _ARG_0_._timer_flash.g, math.abs(math.sin((_ARG_0_._flash_period + coroutine.yield()) * 360 * _ARG_0_._flash_period / 4)) + _ARG_0_._timer_flash.b))
		_ARG_0_._timer_text:set_color(Color(math.abs(math.sin((_ARG_0_._flash_period + coroutine.yield()) * 360 * _ARG_0_._flash_period / 4)) + _ARG_0_._text_flash.a, math.abs(math.sin((_ARG_0_._flash_period + coroutine.yield()) * 360 * _ARG_0_._flash_period / 4)) + _ARG_0_._text_flash.r, math.abs(math.sin((_ARG_0_._flash_period + coroutine.yield()) * 360 * _ARG_0_._flash_period / 4)) + _ARG_0_._text_flash.g, math.abs(math.sin((_ARG_0_._flash_period + coroutine.yield()) * 360 * _ARG_0_._flash_period / 4)) + _ARG_0_._text_flash.b))
	end
end
function HUDTimerElement.remove(_ARG_0_)
	if _ARG_0_._timer_panel then
		_ARG_0_._timer_panel:parent():remove(_ARG_0_._timer_panel)
		_ARG_0_._timer_panel = nil
	end
end
HUDTimerManager._managers = {}
function HUDTimerManager.init(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	_ARG_0_._hud_panel = _ARG_1_.panel
	if _ARG_0_._hud_panel:child("hud_timer_panel") then
		_ARG_0_._hud_panel:remove(_ARG_0_._hud_panel:child("hud_timer_panel"))
	end
	_ARG_0_._timer_panels = {}
	_ARG_3_ = _ARG_3_ or 48
	_ARG_2_ = _ARG_2_ or 65
	_ARG_0_._panel_h = _ARG_3_ + 40
	_ARG_0_._root_panel = _ARG_0_._hud_panel:panel({
		name = "hud_timer_panel",
		h = _ARG_0_._hud_panel:h() - _ARG_2_,
		y = _ARG_2_,
		valign = "top",
		layer = 0,
		visible = true
	})
	_ARG_0_._timer_panels[0] = {
		_timer_panel = {
			right = function()
				return 0
			end
		}
	}
	_ARG_0_._timer_s = _ARG_3_
	_ARG_0_._timer_w = 64
	_ARG_0_._timer_y = _ARG_2_
	table.insert(_ARG_0_._managers, _ARG_0_)
end
function HUDTimerManager.add_timer(_ARG_0_, _ARG_1_, ...)
	_ARG_1_ = _ARG_1_ or ""
	HUDTimerElement:new(_ARG_0_._root_panel, #_ARG_0_._timer_panels + 1, _ARG_0_._timer_s, _ARG_0_._panel_h, _ARG_0_._timer_w, 0, _ARG_1_, ...)._savedParams = {
		_ARG_1_,
		...
	}
	HUDTimerElement:new(_ARG_0_._root_panel, #_ARG_0_._timer_panels + 1, _ARG_0_._timer_s, _ARG_0_._panel_h, _ARG_0_._timer_w, 0, _ARG_1_, ...)._manager = _ARG_0_
	table.insert(_ARG_0_._timer_panels, (HUDTimerElement:new(_ARG_0_._root_panel, #_ARG_0_._timer_panels + 1, _ARG_0_._timer_s, _ARG_0_._panel_h, _ARG_0_._timer_w, 0, _ARG_1_, ...)))
	_ARG_0_:_layout_panels()
	return (HUDTimerElement:new(_ARG_0_._root_panel, #_ARG_0_._timer_panels + 1, _ARG_0_._timer_s, _ARG_0_._panel_h, _ARG_0_._timer_w, 0, _ARG_1_, ...))
end
function HUDTimerManager.del_timer(_ARG_0_, _ARG_1_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._timer_panels) do
		if _FORV_7_._timer_panel == _ARG_1_._timer_panel then
			_FORV_7_:remove()
			table.remove(_ARG_0_._timer_panels, _FORV_6_)
			_ARG_0_:_layout_panels()
			return
		end
	end
end
function HUDTimerManager.receive_timer_migration(_ARG_0_, _ARG_1_, _ARG_2_)
	_ARG_0_._root_panel:parent():parent():set_alpha(1)
	_ARG_1_:init(_ARG_0_._root_panel, #_ARG_0_._timer_panels + 1, _ARG_0_._timer_s, _ARG_0_._panel_h, _ARG_0_._timer_w, 0, unpack(_ARG_2_))
	_ARG_1_._manager = _ARG_0_
	table.insert(_ARG_0_._timer_panels, _ARG_1_)
	_ARG_0_:_layout_panels()
	_ARG_0_._root_panel:parent():parent():set_alpha((_ARG_0_._root_panel:parent():parent():alpha()))
end
function HUDTimerManager.migrate_timer(_ARG_0_, _ARG_1_)
	for _FORV_5_, _FORV_6_ in ipairs(_ARG_0_._managers) do
		if _FORV_6_ ~= _ARG_0_ then
			_ARG_0_:del_timer(_ARG_1_)
			_FORV_6_:receive_timer_migration(_ARG_1_, _ARG_1_._savedParams)
			break
		end
	end
end
function HUDTimerManager._layout_panels(_ARG_0_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._timer_panels) do
		if _FORV_7_._timer_panel and _FORV_7_._timer_panel:visible() then
			if _ARG_0_._timer_panels[0]._timer_panel:right() + _FORV_7_._timer_panel:w() > _ARG_0_._root_panel:w() then
				_FORV_7_._timer_panel:set_x(0)
				_FORV_7_._timer_panel:set_y(0 + _FORV_7_._timer_panel:h())
			else
				_FORV_7_._timer_panel:set_x(_ARG_0_._timer_panels[0]._timer_panel:right() + 1)
				_FORV_7_._timer_panel:set_y(0 + _FORV_7_._timer_panel:h())
			end
		end
	end
end
function HUDTimerManager.root_panel(_ARG_0_)
	return _ARG_0_._root_panel
end
