function CoreMusicManager.set_volume(_ARG_0_, _ARG_1_, ...)
	if IngameWaitingForPlayersState and IngameWaitingForPlayersState._custom_music then
		IngameWaitingForPlayersState._custom_music:set_volume(_ARG_1_ * 100)
	end
	return _UPVALUE0_.set_volume(_ARG_0_, _ARG_1_, ...)
end
