function LootDropScreenGui.init(_ARG_0_, ...)
	_ARG_0_._auto_continue = managers.user:get_setting("hoxhud_autocontinue_screens")
	_ARG_0_._auto_pick = managers.user:get_setting("hoxhud_autopick_lootdrop_card")
	_ARG_0_._shown_t = 0
	return _UPVALUE0_.init(_ARG_0_, ...)
end
function LootDropScreenGui.update(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	if _ARG_0_._auto_pick then
		_ARG_0_._time_left = 0
		_ARG_0_._fade_time_left = nil
		_ARG_0_._is_alone = false
	end
	_ARG_0_._shown_t = not _ARG_0_._button_not_clickable and _ARG_0_._shown_t + _ARG_2_ or _ARG_0_._shown_t
	if _ARG_0_._auto_continue then
		if _ARG_0_._shown_t > (tweak_data.hoxhud.lootdrop_card_delay or 1.5) then
			return _ARG_0_:continue_to_lobby()
		end
	end
	return _UPVALUE0_.update(_ARG_0_, _ARG_1_, _ARG_2_, ...)
end
