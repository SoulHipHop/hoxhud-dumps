function HUDHitConfirm.init(_ARG_0_, ...)
	_UPVALUE0_.init(_ARG_0_, ...)
	_ARG_0_._headshot_confirm = _ARG_0_._hud_panel:panel({
		name = "headshot_confirm",
		layer = 2,
		visible = true,
		x = 0,
		y = 0,
		h = _ARG_0_._hud_panel:h(),
		w = _ARG_0_._hud_panel:w()
	})
	_ARG_0_._headshot_confirm:bitmap({
		texture_rect = {
			15,
			72,
			41,
			112
		},
		valign = "top",
		halign = "center",
		texture = "guis/textures/pd2/hitdirection",
		color = Color.white,
		blend_mode = "add",
		h = 16,
		w = 16,
		visible = false,
		rotation = -90
	}):set_center(_ARG_0_._hud_panel:w() / 2, _ARG_0_._hud_panel:h() / 2)
	_ARG_0_._headshot_confirm:bitmap({
		texture_rect = {
			15,
			72,
			41,
			112
		},
		valign = "center",
		halign = "right",
		texture = "guis/textures/pd2/hitdirection",
		color = Color.white,
		blend_mode = "add",
		h = 16,
		w = 16,
		visible = false,
		rotation = 0
	}):set_center(_ARG_0_._hud_panel:w() / 2, _ARG_0_._hud_panel:h() / 2)
	_ARG_0_._headshot_confirm:bitmap({
		texture_rect = {
			15,
			72,
			41,
			112
		},
		valign = "bottom",
		halign = "center",
		texture = "guis/textures/pd2/hitdirection",
		color = Color.white,
		blend_mode = "add",
		h = 16,
		w = 16,
		visible = false,
		rotation = 90
	}):set_center(_ARG_0_._hud_panel:w() / 2, _ARG_0_._hud_panel:h() / 2)
	_ARG_0_._headshot_confirm:bitmap({
		texture_rect = {
			15,
			72,
			41,
			112
		},
		valign = "center",
		halign = "left",
		texture = "guis/textures/pd2/hitdirection",
		color = Color.white,
		blend_mode = "add",
		h = 16,
		w = 16,
		visible = false,
		rotation = 180
	}):set_center(_ARG_0_._hud_panel:w() / 2, _ARG_0_._hud_panel:h() / 2)
	_ARG_0_._headshot_confirm_sides = {
		_ARG_0_._headshot_confirm:bitmap({
			texture_rect = {
				15,
				72,
				41,
				112
			},
			valign = "top",
			halign = "center",
			texture = "guis/textures/pd2/hitdirection",
			color = Color.white,
			blend_mode = "add",
			h = 16,
			w = 16,
			visible = false,
			rotation = -90
		}),
		_ARG_0_._headshot_confirm:bitmap({
			texture_rect = {
				15,
				72,
				41,
				112
			},
			valign = "center",
			halign = "right",
			texture = "guis/textures/pd2/hitdirection",
			color = Color.white,
			blend_mode = "add",
			h = 16,
			w = 16,
			visible = false,
			rotation = 0
		}),
		_ARG_0_._headshot_confirm:bitmap({
			texture_rect = {
				15,
				72,
				41,
				112
			},
			valign = "bottom",
			halign = "center",
			texture = "guis/textures/pd2/hitdirection",
			color = Color.white,
			blend_mode = "add",
			h = 16,
			w = 16,
			visible = false,
			rotation = 90
		}),
		(_ARG_0_._headshot_confirm:bitmap({
			texture_rect = {
				15,
				72,
				41,
				112
			},
			valign = "center",
			halign = "left",
			texture = "guis/textures/pd2/hitdirection",
			color = Color.white,
			blend_mode = "add",
			h = 16,
			w = 16,
			visible = false,
			rotation = 180
		}))
	}
end
function HUDHitConfirm.on_headshot_confirmed(_ARG_0_)
	for _FORV_4_, _FORV_5_ in ipairs(_ARG_0_._headshot_confirm_sides) do
		_FORV_5_:stop()
		_FORV_5_:animate(callback(_ARG_0_, _ARG_0_, "_animate_show_col"), callback(_ARG_0_, _ARG_0_, "show_done"), 0.25)
	end
end
function HUDHitConfirm._animate_show_col(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	_ARG_1_:set_visible(true)
	_ARG_1_:set_alpha(1)
	while _ARG_3_ > 0 do
		_ARG_1_:set_color(Color((_ARG_3_ - coroutine.yield()) / _ARG_3_, 0, 0))
	end
	_ARG_1_:set_visible(false)
	_ARG_2_()
end
