function ConnectionNetworkHandler.sync_outfit(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	if _ARG_0_._verify_sender(_ARG_3_) and managers.hud then
		managers.hud:set_slot_detection(_ARG_0_._verify_sender(_ARG_3_):id(), _ARG_1_)
	end
	return _UPVALUE0_.sync_outfit(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
end
