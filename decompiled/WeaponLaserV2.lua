function WeaponLaser.init(_ARG_0_, ...)
	_UPVALUE0_.init(_ARG_0_, ...)
	if tweak_data.hoxhud.weapon_laser_color == "fabulousputin" then
		_ARG_0_.update = _ARG_0_.fabulousputin
		_ARG_0_._current_theme = "hoxhud"
	else
		_ARG_0_._themes.hoxhud = tweak_data.hoxhud.weapon_laser_color or {
			light = (Color[managers.user:get_setting("hoxhud_wpn_laser_color")] or Color.green) * (managers.user:get_setting("hoxhud_wpn_laser_light") or 10),
			glow = (Color[managers.user:get_setting("hoxhud_wpn_laser_color")] or Color.green) / (managers.user:get_setting("hoxhud_wpn_laser_glow") or 5),
			brush = Color[managers.user:get_setting("hoxhud_wpn_laser_color")] or Color.green:with_alpha(managers.user:get_setting("hoxhud_wpn_laser_beam") or 0.05)
		}
		_ARG_0_:set_color_by_theme("hoxhud")
	end
end
function WeaponLaser.set_color_by_theme(_ARG_0_, _ARG_1_, ...)
	_ARG_0_._current_theme = _ARG_1_
	return _UPVALUE0_.set_color_by_theme(_ARG_0_, _ARG_1_, ...)
end
function WeaponLaser.fabulousputin(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	_UPVALUE0_.update(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	if _ARG_0_._current_theme ~= "hoxhud" then
		return
	end
	_ARG_0_._themes.hoxhud = {
		light = Color(math.sin(135 * _ARG_2_ + 0) / 2 + 0.5, math.sin(140 * _ARG_2_ + 60) / 2 + 0.5, math.sin(145 * _ARG_2_ + 120) / 2 + 0.5) * 10,
		glow = Color(math.sin(135 * _ARG_2_ + 0) / 2 + 0.5, math.sin(140 * _ARG_2_ + 60) / 2 + 0.5, math.sin(145 * _ARG_2_ + 120) / 2 + 0.5) / 2,
		brush = Color(0.5, math.sin(135 * _ARG_2_ + 0) / 2 + 0.5, math.sin(140 * _ARG_2_ + 60) / 2 + 0.5, math.sin(145 * _ARG_2_ + 120) / 2 + 0.5)
	}
	_ARG_0_:set_color_by_theme("hoxhud")
end
