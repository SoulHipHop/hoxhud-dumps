HoxHudTweakData = class()
HoxHudTweakData.local_strings = Localizer.hoxhud_root
setfenv(0, {
	string = (function(_ARG_0_)
		for _FORV_5_, _FORV_6_ in pairs(getmetatable(_ARG_0_) or _ARG_0_) do
		end
		return setmetatable({}, {
			[_FORV_5_] = function(...)
				return (_UPVALUE1_((_UPVALUE0_(...))) == "userdata" or _UPVALUE1_((_UPVALUE0_(...))) == "table") and _UPVALUE2_((_UPVALUE0_(...))) or _UPVALUE0_(...)
			end,
			["__index"] = _ARG_0_,
			["__newindex"] = function()
			end,
			["__metatable"] = false
		})
	end)(string),
	math = (function(_ARG_0_)
		for _FORV_5_, _FORV_6_ in pairs(getmetatable(_ARG_0_) or _ARG_0_) do
		end
		return setmetatable({}, {
			[_FORV_5_] = function(...)
				return (_UPVALUE1_((_UPVALUE0_(...))) == "userdata" or _UPVALUE1_((_UPVALUE0_(...))) == "table") and _UPVALUE2_((_UPVALUE0_(...))) or _UPVALUE0_(...)
			end,
			["__index"] = _ARG_0_,
			["__newindex"] = function()
			end,
			["__metatable"] = false
		})
	end)(math),
	Color = Color,
	Vector3 = Vector3,
	Rotation = Rotation,
	HoxHudTweakData = HoxHudTweakData,
	dofiles = dofiles,
	packages = packages,
	GameLanguage = SystemInfo:language():key(),
	require = require,
	StrToIDStr = function(_ARG_0_)
		if _UPVALUE0_(_ARG_0_) ~= "string" then
			return ""
		end
		return _UPVALUE1_(_ARG_0_):key()
	end,
	HoxHudPath = HoxHudPath or "HoxHud/"
})
require((HoxHudPath or "HoxHud/") .. "HoxHudTweakData")
setfenv(0, (getfenv(0)))
for _FORV_10_, _FORV_11_ in pairs(HoxHudTweakData) do
	if type(_FORV_11_) == "function" then
		HoxHudTweakData[_FORV_10_] = function(...)
			_UPVALUE0_(1, {
				string = _UPVALUE1_(string),
				math = _UPVALUE1_(math),
				Vector3 = _UPVALUE1_(Vector3),
				Rotation = _UPVALUE1_(Rotation),
				Color = _UPVALUE1_(Color),
				setfenv = _UPVALUE0_
			})
			return _UPVALUE2_(...)
		end
	end
end
function TweakData.init(_ARG_0_, ...)
	_UPVALUE0_.init(_ARG_0_, ...)
	_ARG_0_.hoxhud = HoxHudTweakData:new()
	for _FORV_5_, _FORV_6_ in ipairs(_ARG_0_.hoxhud.CUSTOM_COLORS) do
		if _UPVALUE1_(_FORV_6_.color) == "string" and not Color[_FORV_6_.name] then
			Color[_FORV_6_.name] = Color(_FORV_6_.color)
			if not _ARG_0_.hoxhud.local_strings["menu_color_" .. _FORV_6_.name] then
				_ARG_0_.hoxhud.local_strings["menu_color_" .. _FORV_6_.name] = tostring(_FORV_6_.nice_name)
			end
		end
	end
end
tweak_data = TweakData:new()
