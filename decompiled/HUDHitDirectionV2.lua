HUDHitIndicator = class()
function HUDHitIndicator.init(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_)
	HUDHitIndicator._s_col = HUDHitIndicator._s_col or Color[managers.user:get_setting("hoxhud_hit_ind_start_col")]
	HUDHitIndicator._f_col = HUDHitIndicator._f_col or Color[managers.user:get_setting("hoxhud_hit_ind_fade_col")]
	_ARG_0_._manager = _ARG_1_
	_ARG_0_._dir_ray = _ARG_3_
	_ARG_0_._campos = _ARG_4_:position()
	_ARG_0_._camera = _ARG_4_:forward()
	_ARG_0_._tw = _ARG_2_:bitmap({
		visible = true,
		texture = "guis/textures/pd2/hitdirection",
		color = _ARG_0_:get_hit_color(),
		blend_mode = "add",
		alpha = 0
	}):texture_width()
	_ARG_0_._th = _ARG_2_:bitmap({
		visible = true,
		texture = "guis/textures/pd2/hitdirection",
		color = _ARG_0_:get_hit_color(),
		blend_mode = "add",
		alpha = 0
	}):texture_height()
	_ARG_0_._prevrot = _ARG_0_._camera:to_polar_with_reference(_ARG_0_._dir_ray, math.UP).spin + 180
	_ARG_2_:bitmap({
		visible = true,
		texture = "guis/textures/pd2/hitdirection",
		color = _ARG_0_:get_hit_color(),
		blend_mode = "add",
		alpha = 0
	}):animate(callback(_ARG_0_, _ARG_0_, "_animate"), "test")
end
function HUDHitIndicator.get_hit_color(_ARG_0_)
	return Color(_ARG_0_._s_col.r * (managers.player:player_unit():character_damage():get_real_armor() / managers.player:player_unit():character_damage():_max_armor() * (tweak_data.hoxhud.hit_indicator_damage_bias or 0.8)) + _ARG_0_._f_col.r * (1 - managers.player:player_unit():character_damage():get_real_armor() / managers.player:player_unit():character_damage():_max_armor() * (tweak_data.hoxhud.hit_indicator_damage_bias or 0.8)), _ARG_0_._s_col.g * (managers.player:player_unit():character_damage():get_real_armor() / managers.player:player_unit():character_damage():_max_armor() * (tweak_data.hoxhud.hit_indicator_damage_bias or 0.8)) + _ARG_0_._f_col.g * (1 - managers.player:player_unit():character_damage():get_real_armor() / managers.player:player_unit():character_damage():_max_armor() * (tweak_data.hoxhud.hit_indicator_damage_bias or 0.8)), _ARG_0_._s_col.b * (managers.player:player_unit():character_damage():get_real_armor() / managers.player:player_unit():character_damage():_max_armor() * (tweak_data.hoxhud.hit_indicator_damage_bias or 0.8)) + _ARG_0_._f_col.b * (1 - managers.player:player_unit():character_damage():get_real_armor() / managers.player:player_unit():character_damage():_max_armor() * (tweak_data.hoxhud.hit_indicator_damage_bias or 0.8)))
end
function HUDHitIndicator.clamp_angle(_ARG_0_, _ARG_1_, _ARG_2_)
	return (_ARG_1_ + ((_ARG_0_ - _ARG_1_ + 180) % 360 - 180) - math.min(_ARG_2_, -math.min(_ARG_2_, -((_ARG_0_ - _ARG_1_ + 180) % 360 - 180)))) % 360
end
function HUDHitIndicator.update_position(_ARG_0_, _ARG_1_)
	_ARG_0_._prevrot = _ARG_0_.clamp_angle(_ARG_0_._prevrot, _ARG_0_._camera:to_polar_with_reference(_ARG_0_._dir_ray, math.UP).spin + 180, 6)
	_ARG_1_:set_rotation(_ARG_0_.clamp_angle(_ARG_0_._prevrot, _ARG_0_._camera:to_polar_with_reference(_ARG_0_._dir_ray, math.UP).spin + 180, 6) - 90)
	_ARG_1_:set_x(math.cos(_ARG_0_.clamp_angle(_ARG_0_._prevrot, _ARG_0_._camera:to_polar_with_reference(_ARG_0_._dir_ray, math.UP).spin + 180, 6) - 90) * (_ARG_1_:parent():w() / 2) + _ARG_0_._tw * 1.5)
	_ARG_1_:set_y(math.sin(_ARG_0_.clamp_angle(_ARG_0_._prevrot, _ARG_0_._camera:to_polar_with_reference(_ARG_0_._dir_ray, math.UP).spin + 180, 6) - 90) * (_ARG_1_:parent():h() / 2))
end
function HUDHitIndicator._animate(_ARG_0_, _ARG_1_)
	_ARG_1_:set_alpha(1)
	while managers.user:get_setting("hoxhud_hit_ind_fade_duration") > 0 and not _ARG_0_._cull do
		_ARG_1_:set_color(_ARG_0_:get_hit_color():with_alpha((managers.user:get_setting("hoxhud_hit_ind_fade_duration") - coroutine.yield()) / managers.user:get_setting("hoxhud_hit_ind_fade_duration")))
		_ARG_0_:update_position(_ARG_1_)
	end
	_ARG_0_:remove(_ARG_1_)
	_ARG_0_._manager:remove(_ARG_1_)
end
function HUDHitIndicator.remove(_ARG_0_, _ARG_1_)
	_ARG_1_:parent():remove(_ARG_1_)
	_ARG_0_._manager:remove(_ARG_0_)
end
function HUDHitIndicator.trigger_cull(_ARG_0_)
	_ARG_0_._cull = true
end
function HUDHitIndicator.culled(_ARG_0_)
	return _ARG_0_._cull
end
function HUDHitDirection.init(_ARG_0_, ...)
	_UPVALUE0_.init(_ARG_0_, ...)
	_ARG_0_._hit_direction_panel:clear()
	_ARG_0_._hit_direction_panel:set_h(managers.user:get_setting("hoxhud_hit_ind_box_diameter"))
	_ARG_0_._hit_direction_panel:set_w(managers.user:get_setting("hoxhud_hit_ind_box_diameter"))
	_ARG_0_._indicators_counter = {}
	_ARG_0_._hit_indicator_limit = managers.user:get_setting("hoxhud_hit_ind_max_num")
end
function HUDHitDirection.on_hit_direction(_ARG_0_, _ARG_1_, _ARG_2_)
	if type(_ARG_1_) == "string" or not _ARG_2_ then
		return
	end
	if #_ARG_0_._indicators_counter > _ARG_0_._hit_indicator_limit then
		_ARG_0_:cull_indicator()
	end
	table.insert(_ARG_0_._indicators_counter, (HUDHitIndicator:new(_ARG_0_, _ARG_0_._hit_direction_panel, _ARG_1_, _ARG_2_)))
end
function HUDHitDirection.cull_indicator(_ARG_0_)
	table.remove(_ARG_0_._indicators_counter, 1):trigger_cull()
end
function HUDHitDirection.remove(_ARG_0_, _ARG_1_)
	if not _ARG_1_:culled() then
		table.remove(_ARG_0_._indicators_counter, 1)
	end
end
